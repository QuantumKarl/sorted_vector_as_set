# README #

### What is it? ###
It performs similar operations to std::set / std::unordered_set.
In addition, it provides overloaded operators (|,-,&,^,<=,<,>=,>) to perform set (theory) operations (union,difference,intersection,symmetric_difference, sub-set, proper sub-set, super-set, proper super-set).

### Why does it exist? ###
After reading this paper [Why you shouldn't use set by Matt Austern](http://lafstern.org/matt/col1.pdf) I thought if the factiods are true, I could use it to improve / standardise set (theory) operations in other projects.

### How do I use it? ###
See src/FunctionalityTests.hpp, src/CompilationTests.hpp and src/Benchmark.hpp you can see it in use.

### Is it faster than std::unordered_set? ###
* Yes, averaged its ~8.5 times faster. Best case ~40 times (operations on linux@65K), worst case ~1 times faster (inserting on linux@4K).

See the data and more exciting graphs here: data/win_x64/Test3/Graphs.xlsx and data/debian_x64/Test3/Graphs.xlsx.

![how_many_times_faster.png](https://bitbucket.org/repo/M8bkzn/images/436257639-how_many_times_faster.png)

Note: This data was created by src/Benchmark.hpp, which uses doubles and int64_t types **only**. For other types such as std::string performance will change.

### Why is it faster? ###
Well, a solution to set operations is to use a hash table to represent your set, then do look ups. This gives a lovely O(n) operation (this is also python's solution). However, another solution is to keep the data sorted, and compare a value from each set. If one is smaller than the other, that set's current index gets incremented (then repeat until both sets are traversed). This yields a O( 2(n+m)-1 ) operation (this is what STL does). Now, O(2(n+m)-1) is worse than O(n), however, the O(2(n+m)-1) is much faster than the O(n). Why!? because the O(n) is counting hashes / look ups. The O(2(n+m)-1) is counting comparisons. How many interger comparisons can you do for one integer hash look up? Of course it depends on the hash and the hardware, but in sort; a lot!

### How do I get set up? ###
Use the **make file** (GCC) or the **MSVS 2015 sln**.

### Does it have any dependencies? ###
* vec_set class requires STL, C++14.

The tests have dependencies on:


* [Eigen Linear algebra library](http://eigen.tuxfamily.org/index.php?title=Main_Page)(non-pod test / allocators test)
* [Boost](http://www.boost.org)(non-pod test / allocators test)
* [python](https://www.python.org)(compared functionality test)

### Is it memory safe? ###
Yes, it has been tested with:

* [CppCheck](http://cppcheck.sourceforge.net).
* [Valgrind](http://valgrind.org/docs/manual/mc-manual.html).
* [Callgrind](http://valgrind.org/docs/manual/cl-manual.html).


No errors or problems detected.

### What is next? ###
Perhaps further benchmarking with non pod data types such as std::string.
Maybe SIMD operations like in these papers; [Fast Sorted-Set Intersection using SIMD Instructions](http://www.adms-conf.org/p1-SCHLEGEL.pdf) or [Faster Set Intersection with SIMD instructions
by Reducing Branch Mispredictions](http://www.vldb.org/pvldb/vol8/p293-inoue.pdf)
