"""
Simple script that outputs the result of set operations.
Takes the arguments:
SetSize, SetSizeRatio, DataRange, Seed
where:
    SetSize tells the script how many times to call rand for inserting elements into the largest set
    SetSizeRatio determins the size of the second set
    DataRange is the range of the value which are going into the set
    Seed is the source for the random number generator's seed value
"""

#!/usr/bin/python
import sys
import random
import math

#fill in a set with random ints
def fillSet( wantedSize, maxValue ):
    s = set()
    count = 0
    while( count < wantedSize ):
        num = random.randint(0,maxValue)
        s.add(num)
        count += 1
        
    return s

#print a set nicely
#NOTE: in sorted order
def prettyPrint( s, lable ):
    l = list(s)
    l.sort()
    output = str(l)
    return lable + ": " + output

#print a bool nicely
def prettyBoolPrint( b, lable ):
    output = str(b)
    return lable + ": " + output

try:
    #count args
    numArgs = len(sys.argv)

    #check quantity of args
    if numArgs == 5:
        #extract args
        maxSetSize = int(str(sys.argv[1]))
        setSizeRatio = float(str(sys.argv[2]))
        setDataRange = int(str(sys.argv[3]))
        seedValue = int(str(sys.argv[4]))

        #seed PRNG
        random.seed(seedValue)

        #fill the sets
        a = fillSet(maxSetSize, setDataRange)
        b = fillSet(math.floor(maxSetSize*setSizeRatio), setDataRange)

        #output the results of set operations
        print (prettyPrint(a, "A")) #set A
        print (prettyPrint(b, "B")) #set B
        print (prettyPrint(a|b, "A|B")) #union
        print (prettyPrint(a&b, "A&B")) #intersection
        print (prettyPrint(a-b, "A-B")) #difference
        print (prettyPrint(b-a, "B-A")) #difference
        print (prettyPrint(a^b, "A^B")) #Symmetric Difference
        
        print (prettyBoolPrint(a<=b, "a<=b")) #sub-set
        print (prettyBoolPrint(a>=b, "a>=b")) #super-set
        print (prettyBoolPrint( (a&b) <= a, "(a&b) <= a" )) #always sub-set
        print (prettyBoolPrint( (a|b) >= a, "(a|b) >= a" )) #always super-set
        print (prettyBoolPrint(a<b, "a<b")) #proper sub-set
        print (prettyBoolPrint(a>b, "a>b")) #proper super-set
        print (prettyBoolPrint( (a&b) < a, "(a&b) < a" )) #always sub-set
        print (prettyBoolPrint( (a|b) > a, "(a|b) > a" )) #always super-set
    else:
        print ("Incorrect number of arguements, 4 is expected. eg: SetOps 14 2 47 1077")
except:
    print ("Unexpected error:", sys.exc_info()[0])
    raise
