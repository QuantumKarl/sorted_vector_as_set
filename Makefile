
srcDir = src
objDir = build
outDir = bin/gcc
appname := SortedVector

CXX := g++
CXXFLAGS := -g -std=gnu++14 -Wall -Wextra -Wpedantic -Wconversion -O3
LDLIBS := -lboost_system

STRUCTURE := $(shell find $(srcDir) -type d)
CODEFILES := $(addsuffix /*,$(STRUCTURE))
CODEFILES := $(wildcard $(CODEFILES))

srcfiles := $(filter %.cpp,$(CODEFILES))
objects  := $(patsubst %.cpp, %.o, $(srcfiles))

all: $(appname)

$(appname): $(objects)
	$(CXX) -v $(CXXFLAGS) $(LDFLAGS) -o $(appname) $(objects) $(LDLIBS)

clean:
	rm -f $(objects)

print-% : ; $(info $* is $(flavor $*) variable set to [$($*)]) @true
