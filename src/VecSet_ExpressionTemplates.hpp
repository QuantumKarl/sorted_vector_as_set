#if !defined( D_VEC_SET_EXPRESSION_TEMPLATES )
#define D_VEC_SET_EXPRESSION_TEMPLATES


#define D_USING_EXPRESSION_TEMPLATES 0
//--------------------------------
/* this block would be before VecSet's class definition
*/
#if 0
//------------------------------------------------------
#if defined( D_USING_EXPRESSION_TEMPLATES ) && ( D_USING_EXPRESSION_TEMPLATES == 1)
namespace detail
{
	//--------------------------------
	/* exp
	*/
	template< typename data_type, typename allocator_type, typename comparison_type >
	class exp final
	{
	public:
		//--------------------------------
		/* exp( i_result, i_cmp )
		i_result should contain a vector containing the result of a calculation,
		i_cmp should contain a comparison class instance.
		*/
		template< typename T1, typename T2 >
		exp( T1&& i_result, T2&& i_cmp ) noexcept
			:
		res( forward<T1>( i_result ) ),
			cmp( forward<T2>( i_cmp ) )
		{ }

		//-------- aliases --------
		using this_type = exp< data_type, allocator_type, comparison_type >;

		/*-------- delete undesired functions --------
		moving is allowed,
		copying is not allowed.
		*/
		exp() = delete; // only to be constructed with provided ctor
		~exp() noexcept = default; // default dtor acceptable
		exp( this_type const& i_other ) = delete; // you are not to copy
		exp( this_type&& i_other ) noexcept = default; // you can move

		this_type& operator=( this_type const& i_other ) = delete; // you can not copy / assign
		this_type& operator=( this_type&& i_other ) noexcept = default; // you can move assign

		//-------- members --------
		vector< data_type, allocator_type > res;
		comparison_type cmp;
	};

	//--------------------------------
	/* exp_res
	*/
	template< typename data_type, typename allocator_type, typename comparison_type >
	class exp_res final
	{
	public:
		//--------------------------------
		/* exp_res( i_src, i_temp, i_cmp )
		i_src should contain the result of a calculation (i.e. the source data for another calculation)
		i_temp should contain anything (as long as its not the same container as the source)
		i_cmp should contain a comparison class instance
		*/
		template< typename T1, typename T2, typename T3 >
		exp_res( T1&& i_src, T2&& i_temp, T3&& i_cmp ) noexcept
			:
		src( forward<T1>( i_src ) ),
			temp( forward<T2>( i_temp ) ),
			cmp( forward<T3>( i_cmp ) )
		{ }

		//-------- aliases --------
		using this_type = exp_res< data_type, allocator_type, comparison_type >;

		/*-------- delete undesired functions --------
		moving is allowed,
		copying is not allowed.
		*/
		exp_res() = delete; // only to be constructed with provided ctor
		~exp_res() noexcept = default; // default dtor acceptable
		exp_res( this_type const& i_other ) = delete; // you are not to copy
		exp_res( this_type&& i_other ) noexcept = default; // you can move

		this_type& operator=( this_type const& i_other ) = delete; // you can not copy / assign
		this_type& operator=( this_type&& i_other ) noexcept = default; // you can move assign

		//-------- members --------
		vector< data_type, allocator_type > src;
		vector< data_type, allocator_type > temp;
		comparison_type cmp;
	};
}
#endif
//------------------------------------------------------
#endif

//--------------------------------
/* this block would be inside the vec_set class (defining operators)
*/
#if 0

#if defined( D_USING_EXPRESSION_TEMPLATES ) && ( D_USING_EXPRESSION_TEMPLATES == 1)
//--------------------------------
/* ctor( detail::exp )

*/
template< typename alloc_t >
vec_set( detail::exp< value_type, alloc_t, compare_t >&& i_rhs ) noexcept
	:
m_v( move( i_rhs.res ) ),
m_cmp( move( i_rhs.cmp ) ),
m_eql( equal_t() )
{ }


//--------------------------------
/* ctor( detail::exp )

*/
vec_set( detail::exp< value_type, allocator_type, compare_t >&& i_rhs, allocator_type const& i_allocator ) noexcept
	:
m_v( move( i_rhs.res ), i_allocator ),
m_cmp( move( i_rhs.cmp ) ),
m_eql()
{ }

//--------------------------------
/* ctor( detail::exp_res )

*/
template< typename alloc_t >
vec_set( detail::exp_res< value_type, alloc_t, compare_t >&& i_rhs ) noexcept
	:
m_v( move( i_rhs.src ) ),
m_cmp( move( i_rhs.cmp ) ),
m_eql()
{ }


//--------------------------------
/* ctor( detail::exp_res )

*/
vec_set( detail::exp_res< value_type, allocator_type, compare_t >&& i_rhs, allocator_type const& i_allocator ) noexcept
	:
m_v( move( i_rhs.src ), i_allocator ),
m_cmp( move( i_rhs.cmp ) ),
m_eql( equal_t() )
{ }

//--------------------------------
/* operator= this_type const&
assign right
*/
template< typename alloc_t >
this_type& operator=( detail::exp< value_type, alloc_t, compare_t >&& i_rhs_exp ) noexcept
{
	//assert( i_rhs.cmp == m_cmp );
	m_v = move( i_rhs_exp.res );

	return *this;
}

template< typename alloc_t >
this_type& operator=( detail::exp_res< value_type, alloc_t, compare_t >&& i_rhs_exp ) noexcept
{
	//assert( i_rhs.cmp == m_cmp );
	m_v = move( i_rhs_exp.src );

	return *this;
}

#endif

#endif

//--------------------------------
// this block would wrap the normal operator definitions
#if 0
#if defined( D_USING_EXPRESSION_TEMPLATES ) && ( D_USING_EXPRESSION_TEMPLATES == 1 )

#include "VecSet_ExpressionTemplates.hpp"

#elif defined( D_USING_EXPRESSION_TEMPLATES ) && ( D_USING_EXPRESSION_TEMPLATES == 0 )
#endif
//--------------------------------
/* not in use
	this block would stay in this file
*/
#if 0
namespace detail_exp_temp
{
	//-------------------------------- functions for |,|=,-,-=,&,&=,^ and ^= operators --------------------------------

	//--------------------------------
	/* set_union( i_lhs, i_rhs )
		O( 2(n+m)-1 )
		where:
			n = i_lhs.size()
			m = i_rhs.size()

		NOTE: used in |, and |=
	*/
	template< typename T, typename cmp >
	inline
	void
	set_union( T const& i_lhs, T const& i_rhs, cmp const& i_cmp, T& o_out )
	{
		o_out.resize( i_lhs.size() + i_rhs.size() );// the largest it could be is the sum of both sizes
		auto newEnd = std::set_union( i_lhs.cbegin(), i_lhs.cend(), i_rhs.cbegin(), i_rhs.cend(), o_out.begin(), i_cmp );
		o_out.resize( distance( o_out.begin(), newEnd ) );
	}

	template< typename t, typename c, typename e, typename a>
	inline
	bool
	equal( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
	{
		// check data equality
		auto itrL = i_lhs.begin();
		auto itrR = i_rhs.begin();
		auto itrLEnd = i_lhs.end();
		auto const& eqlcmp = i_lhs.get_equal();
		do
		{
			if( eqlcmp( *itrL, *itrR ) == false )
			{
				return false;
			}
			++itrR;
		} while( ++itrL != itrLEnd );

		// we have reached the end of the data without early-out
		return true;
	}
}

//----------------
/* operator|
	union
	vec_set | vec_set = exp

	NOTE: 1 vector buffer creation.
	NOTE: 0 vector buffer destruction.
*/
template< typename t, typename c, typename e, typename a>
detail::exp<t, a, c> operator|( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	if( &i_lhs == &i_rhs || i_rhs.empty() )
	{
		return detail::exp<t, a, c>( i_lhs._get_cont(), i_lhs.get_compare() );
	}
	else if( i_lhs.empty() )
	{
		return detail::exp<t, a, c>( i_rhs._get_cont(), i_lhs.get_compare() );
	}

	vector<t, a> temp;

	// output the result of union into temp
	detail_exp_temp::set_union( i_lhs._get_cont(), i_rhs._get_cont(), i_lhs.get_compare(), temp );

	return detail::exp<t, a, c>( move( temp ), i_lhs.get_compare() );
}

//--------------------------------
/* operator|
	exp | vec_set = exp_res

	NOTE: 1 vector buffer creation.
	NOTE: 0 vector buffer destruction.
	NOTE: reverse arguments of below.
*/
template< typename t, typename c, typename e, typename a>
detail::exp_res<t, a, c> operator|( detail::exp<t, a, c>&& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	if( i_rhs.empty() )
	{
		return detail::exp_res<t, a, c>( move( i_lhs.res ), vector<t, a>(), move( i_lhs.cmp ) );
	}
	else if( i_lhs.res.empty() )
	{
		return detail::exp_res<t, a, c>( i_rhs._get_cont(), move( i_lhs.res ), move( i_lhs.cmp ) );
	}

	vector<t, a> temp;

	// output the result of union into temp
	detail_exp_temp::set_union( i_lhs.res, i_rhs._get_cont(), i_lhs.cmp, temp );

	// the new temp (temp) becomes the result,
	// the old temp (i_lhs.res) becomes the new temp (where a new result can be written to)
	return detail::exp_res<t, a, c>( move( temp ), move( i_lhs.res ), move( i_lhs.cmp ) );
}

//--------------------------------
/* operator|
	vec_set | exp = exp_res

	NOTE: 1 vector buffer creation.
	NOTE: 0 vector buffer destruction.
	NOTE: reverse arguments of above.
*/
template< typename t, typename c, typename e, typename a>
detail::exp_res<t, a, c> operator|( vec_set<t, c, e, a> const& i_lhs, detail::exp<t, a, c>&& i_rhs )
{
	if( i_rhs.res.empty() )
	{
		return detail::exp_res<t, a, c>( i_lhs._get_cont(), move( i_rhs.res ), move( i_rhs.cmp ) );
	}
	else if( i_lhs.empty() )
	{
		return detail::exp_res<t, a, c>( move( i_rhs.res ), vector<t, a>(), move( i_rhs.cmp ) );
	}

	vector<t, a> temp;

	// output the result of union into temp
	detail_exp_temp::set_union( i_lhs._get_cont(), i_rhs.res, i_rhs.cmp, temp );

	// the new temp (temp) becomes the result,
	// the old temp (i_rhs.res) becomes the new temp (where a new result can be written to)
	return detail::exp_res<t, a, c>( move( temp ), move( i_rhs.res ), move( i_rhs.cmp ) );
}

//--------------------------------
/* operator|
	exp_res | vec_set = exp_res

	NOTE: 0 vector buffer creation.
	NOTE: 0 vector buffer destruction.
	NOTE: reverse arguments of below.
*/
template< typename t, typename c, typename e, typename a>
detail::exp_res<t, a, c> operator|( detail::exp_res<t, a, c>&& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	if( i_rhs.empty() )
	{
		return detail::exp_res<t, a, c>( move( i_lhs.src ), move( i_lhs.temp ), move( i_lhs.cmp ) );
	}
	else if( i_lhs.src.empty() )
	{
		return detail::exp_res<t, a, c>( i_rhs._get_cont(), move( i_lhs.temp ), move( i_lhs.cmp ) );
	}

	// output the result of union into i_lhs.temp
	detail_exp_temp::set_union( i_lhs.src, i_rhs._get_cont(), i_lhs.cmp, i_lhs.temp );

	// the temp becomes the source (it now contains the result)
	// the source becomes the temp (it now contains the previous result, which we don't need)
	return detail::exp_res<t, a, c>( move( i_lhs.temp ), move( i_lhs.src ), move( i_lhs.cmp ) );
}

//--------------------------------
/* operator|
	vec_set | exp_res = exp_res

	NOTE: 0 vector buffer creation.
	NOTE: 0 vector buffer destruction.
	NOTE: reverse arguments of above.
*/
template< typename t, typename c, typename e, typename a>
detail::exp_res<t, a, c> operator|( vec_set<t, c, e, a> const& i_lhs, detail::exp_res<t, a, c>&& i_rhs )
{
	if( i_rhs.src.empty() )
	{
		return detail::exp_res<t, a, c>( i_lhs._get_cont(), move( i_rhs.temp ), move( i_rhs.cmp ) );
	}
	else if( i_lhs.empty() )
	{
		return detail::exp_res<t, a, c>( move( i_rhs.src ), move( i_rhs.temp ), move( i_rhs.cmp ) );
	}

	// output the result of union into i_rhs.temp
	detail_exp_temp::set_union( i_lhs._get_cont(), i_rhs.src, i_rhs.cmp, i_rhs.temp );

	// the temp becomes the source (it now contains the result)
	// the source becomes the temp (it now contains the previous result, which we don't need)
	return detail::exp_res<t, a, c>( move( i_rhs.temp ), move( i_rhs.src ), move( i_rhs.cmp ) );
}

/*-------------------------------- irregular arguments --------------------------------
	The only way to call the following functions is to use brackets in expressions.
	These do cause reduced performance.
	Worst case it is the same number of buffers as not using these expression templates,
	Best case any length expression uses two buffers.
*/

//--------------------------------
/* operator|
	exp | exp = exp_res

	NOTE: 1 vector buffer creation.
	NOTE: 1 vector buffer destruction.
*/
template< typename t, typename a, typename c>
detail::exp_res<t, a, c> operator|( detail::exp<t, a, c>&& i_lhs, detail::exp<t, a, c>&& i_rhs )
{
	assert( &i_lhs != &i_rhs );

	if( i_rhs.res.empty() )
	{
		return detail::exp_res<t, a, c>( move( i_lhs.res ), move( i_rhs.res ), move( i_lhs.cmp ) );
	}
	else if( i_lhs.res.empty() )
	{
		return detail::exp_res<t, a, c>( move( i_rhs.res ), move( i_lhs.res ), move( i_lhs.cmp ) );
	}

	vector<t, a> temp;

	// output the result of union into temp
	detail_exp_temp::set_union( i_lhs.res, i_rhs.res, i_lhs.cmp, temp );

	// the new temp (temp) becomes the result,
	// the old temp (i_rhs.res) becomes the new temp (where a new result can be written to)
	return detail::exp_res<t, a, c>( move( temp ), move( i_lhs.res ), move( i_lhs.cmp ) );
}

//--------------------------------
/* operator|
	exp_res | exp = exp_res

	NOTE: 0 vector buffer creation.
	NOTE: 1 vector buffer destruction.
	NOTE: reverse arguments of below.
*/
template< typename t, typename a, typename c>
detail::exp_res<t, a, c> operator|( detail::exp_res<t, a, c>&& i_lhs, detail::exp<t, a, c>&& i_rhs )
{
	if( i_rhs.res.empty() )
	{
		return detail::exp_res<t, a, c>( move( i_lhs.src ), move( i_lhs.temp ), move( i_lhs.cmp ) );
	}
	else if( i_lhs.src.empty() )
	{
		return detail::exp_res<t, a, c>( move( i_rhs.res ), move( i_lhs.temp ), move( i_lhs.cmp ) );
	}

	// output the result of union into i_lhs.temp
	detail_exp_temp::set_union( i_lhs.src, i_rhs.res, i_rhs.cmp, i_lhs.temp );

	// the temp becomes the source (it now contains the result)
	// the source becomes the temp (it now contains the previous result, which we don't need)
	return detail::exp_res<t, a, c>( move( i_lhs.temp ), move( i_lhs.src ), move( i_lhs.cmp ) );
}

//--------------------------------
/* operator|
	exp | exp_res = exp_res

	NOTE: 0 vector buffer creation.
	NOTE: 1 vector buffer destruction.
	NOTE: reverse arguments of above.
*/
template< typename t, typename a, typename c>
detail::exp_res<t, a ,c> operator|( detail::exp<t, a, c>&& i_lhs, detail::exp_res<t, a, c>&& i_rhs )
{
	if( i_rhs.src.empty() )
	{
		return detail::exp_res<t, a, c>( move( i_lhs.res ), move( i_rhs.temp ), move( i_rhs.cmp ) );
	}
	else if( i_lhs.res.empty() )
	{
		return detail::exp_res<t, a, c>( move( i_rhs.src ), move( i_rhs.temp ), move( i_rhs.cmp ) );
	}

	// output the result of union into i_rhs.temp
	detail_exp_temp::set_union( i_lhs.res, i_rhs.src, i_lhs.cmp, i_rhs.temp );

	// the temp becomes the source (it now contains the result)
	// the source becomes the temp (it now contains the previous result, which we don't need)
	return detail::exp_res<t, a, c>( move( i_rhs.temp ), move( i_rhs.src ), move( i_rhs.cmp ) );
}

//--------------------------------
/* operator|
	exp_res | exp_res = exp_res

	NOTE: 0 vector buffer creation.
	NOTE: 2 vector buffer destruction.
*/
template< typename t, typename a, typename c>
detail::exp_res<t, a, c> operator|( detail::exp_res<t, a, c>&& i_lhs, detail::exp_res<t, a, c>&& i_rhs )
{
	assert( &i_lhs != &i_rhs );

	if( i_rhs.src.empty() )
	{
		return detail::exp_res<t, a, c>( move( i_lhs.src ), move( i_lhs.temp ), move( i_lhs.cmp ) );
	}
	else if( i_lhs.src.empty() )
	{
		return detail::exp_res<t, a, c>( move( i_rhs.src ), move( i_lhs.temp ), move( i_lhs.cmp ) );
	}

	// output the result of union into i_lhs.temp
	detail_exp_temp::set_union( i_lhs.src, i_rhs.src, i_lhs.cmp, i_lhs.temp );

	// the temp becomes the source (it now contains the result)
	// the source becomes the temp (it now contains the previous result, which we don't need)
	return detail::exp_res<t, a, c>( move( i_lhs.temp ), move( i_lhs.src ), move( i_lhs.cmp ) );
}


//--------------------------------
/* operator==
*/
template< typename t, typename c, typename e, typename a>
bool operator==( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	// check if they point to the same object
	if( &i_lhs == &i_rhs ) return true;

	// check sizes
	if( i_lhs.size() != i_rhs.size() ) return false;

	// we have determined they both have the same size, however, both are empty
	if( i_lhs.empty() ) return true;

	return detail_exp_temp::equal( i_lhs, i_rhs );
}
#endif

//--------------------------------
/* this block would go inside the main function
*/
#if 0
void ExpressionTemplatesTest()
{
	vec_set< int64_t > a{ 2,4,6 };
	vec_set< int64_t > b{ 8,10,12 };
	vec_set< int64_t > c{ 14,16,18 };
	vec_set< int64_t > d{ 20,22,24 };
	vec_set< int64_t > e{ 26,28,30 };
	vec_set< int64_t > f{ 32,34,36 };
	vec_set< int64_t > res;

	// simple test
#if 0
	{
		res = a | b | c | d | e;
		cout << res;
	}
#endif
	// attempt to force every combination of functions / types

	res = a | b; // vec_set | vec_set

	res = a | ( b | c ); // vec_set | exp
	res = ( a | b ) | c; // exp | vec_set

	res = a | ( b | c | d ); // vec_set | exp_res
	res = ( a | b | c ) | d; // exp_res | vec_set

	res = ( a | b ) | ( c | d ); // exp | exp
	res = ( a | b | c ) | ( d | e ); // exp_res | exp
	res = ( a | b ) | ( c | d | e ); // exp | exp_res
	res = ( a | b | c ) | ( d | e | f ); // exp_res | exp_res

	res = a; // assign vec_set
	res = a | b; // assign exp
	res = a | b | c; // assign exp_res

	vec_set< int64_t > ctor1 = a; // = vec_set
	vec_set< int64_t > ctor2 = a | b; // exp
	vec_set< int64_t > ctor3 = a | b | c; // = exp_res

	auto notWhatYouWouldExpect1 = a | b; // = exp
	auto notWhatYouWouldExpect2 = a | b | c; // = exp_res

	auto whatYouWouldExpect1 = static_cast<vec_set<int64_t> > ( a | b );

	using greater_set = vec_set< int64_t, std::greater<int64_t> >;
	greater_set gt_set{ 1,2,3,4,4,4,5,6,7,8,9 };
	greater_set gt_set2{ 10,2,3,4,1,23,4 };
	greater_set gt_res = gt_set | gt_set2;
}

#include <sstream>
void expressionTemplates_simple_test( int64_t const i_dataSize, size_t const i_loops, string& o_result )
{
	//---------------- generate random numbers ----------------
	int64_t const minSize = i_dataSize / -2;
	int64_t const maxSize = i_dataSize / 2;
	std::mt19937_64 gen( i_dataSize*i_loops );
	std::uniform_int_distribution<int64_t> distI( minSize, maxSize );

	//---------------- start testing ----------------
	Timer_t tAll;
	Timer_t tInsert;
	Timer_t tOps;
	Timer_t::timeDuration_t tInsertSum( 0 );
	Timer_t::timeDuration_t tOpsSum( 0 );

	tAll.Start();

	cont_t<int64_t> sI1;
	cont_t<int64_t> sI2;
	cont_t<int64_t> sI3;
	cont_t<int64_t> sI4;
	cont_t<int64_t> sI5;
	cont_t<int64_t> sI6;

	cont_t<int64_t> sItemp1;
	cont_t<int64_t> sItemp2;

	for( size_t j = 0; j < i_loops; ++j )
	{
		//---------------- start timing numbers ----------------
		tInsert.Start();
		sI1.clear();
		sI2.clear();
		sI3.clear();
		sI4.clear();
		sI5.clear();
		sI6.clear();
		sItemp1.clear();
		sItemp2.clear();
		for( int64_t i = 0; i < i_dataSize; ++i )
		{
			sI1._push_back( distI( gen ) );
			sI2._push_back( distI( gen ) );
			sI3._push_back( distI( gen ) );
			sI4._push_back( distI( gen ) );
			sI5._push_back( distI( gen ) );
			sI6._push_back( distI( gen ) );
		}

		sI1.validate();
		sI2.validate();
		sI3.validate();
		sI4.validate();
		sI5.validate();
		sI6.validate();

		tInsert.End();
		tInsert.CalcDuration();
		tInsertSum += tInsert.GetDuration();
		//---------------- end timer ----------------

		//---------------- start set operations timer ----------------
		// perform some set operations and check that they are equal
		tOps.Start();

		sItemp1 = sI1 | sI2 | sI3 | sI4 | sI5 | sI6; // faster
		sItemp2 = ( sI1 | sI2 ) | ( sI3 | sI4 ) | ( sI5 | sI6 ); // same
		crashOnFalse( sItemp1 == sItemp2 );

		tOps.End();
		tOps.CalcDuration();
		tOpsSum += tOps.GetDuration();
		//---------------- end timer ----------------
	}
	tAll.End();
	tAll.CalcDuration();

	// size, ran, insert, ops, total
	stringstream ss;
	ss << i_dataSize << ", ";
	ss << tInsertSum.count() << ", ";
	ss << tOpsSum.count() << ", ";
	ss << tAll.GetDuration().count();

	o_result += ss.str();
}

void run_quick_test()
{
	array<size_t, 18> const values = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072 };

	size_t const innerLoopSize = 1024;

	vector<string> res;
	res.push_back( "size, insert, ops, total" );

	cout << "---- Started benchmark ----" << endl;
	for( auto& v : values )
	{
		res.push_back( "" );
		expressionTemplates_simple_test( v, innerLoopSize, res.back() ); // timers are inside this function
	}
	cout << "---- Finished benchmark ----" << endl;

	//---- output to screen ----
	for( auto e : res )
	{
		cout << e << endl;
	}
}

#endif
#endif