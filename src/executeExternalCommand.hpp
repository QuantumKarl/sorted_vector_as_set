#if !defined( D_EXEC_HPP )
#define D_EXEC_HPP
#include "sys_platform.hpp"

#include <assert.h>
#include <memory>
#include <string>

//--------------------------------
/* cross platform
	toggle function names based on platform
*/
#if defined( D_WIN )
	#define D_popen _popen
	#define D_pclose _pclose
#elif defined( D_GNU )
	#define D_popen popen
	#define D_pclose pclose
	#include <cstdio>
#else
	static_assert( false );
#endif

//--------------------------------
/* exec( i_cmd, o_str )
	execute an external command (in terminal / command prompt),
	its output is written to o_str.
*/
static bool exec(std::string const& i_cmd, std::string& o_str)
{
	assert(i_cmd.size() > 0);
	if (i_cmd.size() == 0) return false;

	size_t const bufSize = 256;
	char buffer[256];

	std::shared_ptr<FILE> pipe( D_popen(i_cmd.c_str(), "r"), D_pclose); // warning apparently opens a terminal

	if (!pipe) return false;

	// upper bound of 32mb file reading
	// 1024 bytes * 1024 bytes = 1mb * 32 = 32 mb / 32bytes (the size of one buffer read)
	size_t const upper_bound = (1024 * 1024 * 32) / 32;
	size_t i = 0;

	while (fgets(buffer, bufSize, pipe.get()) != nullptr)
	{
		o_str += buffer;

		// to avoid any chance of softlock Rule 2
		if (i >= upper_bound)
		{
			o_str.clear();
			return false;
		}
		++i;
	}
	return true;
}

#endif
