/*	This file is part of sorted_vector_as_set.
	Copyright(C) 2016 by Karl Wesley Hutchinson

	sorted_vector_as_set is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	sorted_vector_as_set is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with sorted_vector_as_set. If not, see <http://www.gnu.org/licenses/>.
*/

// pick any (both,either,none)
#define D_HAVE_EIGEN 1
#define D_HAVE_BOOST 1

// pick one
#define D_TEST_VEC_SET 1
#define D_TEST_UNORDERD_SET 0

// pick one
#define D_USE_INSERT 0
#define D_USE_BATCH_INSERT 1

// pick any (both,either,none)
#define D_TEST_FUNCIONALITY 1
#define D_RUN_BENCH 0

// toggle to run smaller bench (used as an extended functionality test)
#define D_SMALL_BENCH 0

#include "VecSet.hpp"
#include "executeExternalCommand.hpp"
#include "stl_set_operators.hpp"
#include "sys_platform.hpp"
#include "sys_stringUtils.hpp"
#include "sys_timer.hpp"

#include <array>
#include <fstream>
#include <iostream>
#include <limits>
#include <numeric>
#include <random>
#include <valarray>

//--------------------------------
/* cont_t
	define what container we are interested in testing.
*/
#if defined( D_TEST_VEC_SET ) && (D_TEST_VEC_SET == 1) && (D_TEST_UNORDERD_SET == 0)
	template < class T >
	using cont_t = vec_set<T>;
#elif defined( D_TEST_UNORDERD_SET ) && ( D_TEST_UNORDERD_SET == 1) && (D_TEST_VEC_SET == 0)
	template < class T >
	using cont_t = std::unordered_set<T>;
#else
	static_assert( false, "you need to test something" );
#endif

#include "Benchmark.hpp"
#include "CompilationTests.hpp"
#include "FunctionalityTests.hpp"
#include "writeToCSV.hpp"

int main()
{
#if defined( D_TEST_FUNCIONALITY ) && (D_TEST_FUNCIONALITY == 1)
	IteratorTemplatesTest();
	ConstructorTest();
	MembersTest();
	NonPodTest();

	CoutFunctionality();
	ComparedFunctionalityTest(10,(2.0/3.0),10,256,5,false);
	ComparedFunctionalityTest(100,1.23,123,10,200);
	ComparedFunctionalityTest(1'000,2.0/3.0,1'000,1057,125);

	std::cout << "Tests completed successfully" << std::endl;
#endif

	//---------------- test for graph ----------------
#if defined( D_RUN_BENCH ) && (D_RUN_BENCH == 1)
	//---- estimate error per start/stop call to timers ----
	std::cout << "Timer error per start/stop: " << Timer_t::TestLatency().count() << " milli seconds "<< std::endl;

	size_t const innerLoopSize = 768; // decided from trial and error / isn't too long or short.

#if defined( D_SMALL_BENCH ) && (D_SMALL_BENCH == 1)
	std::array<size_t, 13> const values = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 };
#elif defined( D_SMALL_BENCH ) && (D_SMALL_BENCH == 0)
	std::array<size_t, 17> const values = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536 };
#else
	static_assert( false && "missing #define" );
#endif

	vector<string> res;
	res.push_back("size, ran, insert, ops, total");

	std::cout << "---- Started benchmark ----" << std::endl;
	for (auto& v : values)
	{
		res.push_back("");
		bench(v, innerLoopSize, res.back() ); // timers are inside this function
	}
	std::cout << "---- Finished benchmark ----" << std::endl;

	//---- output to screen ----
	for (auto e : res)
	{
		std::cout << e << std::endl;
	}

	//---- output to file ----
	writeToCSV(res);
	std::cout << "---- Written results to file ----" << std::endl;
#endif

	return 0;
}
