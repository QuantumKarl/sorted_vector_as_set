#if !defined( D_SYS_STRING_UTILS_HPP )
#define D_SYS_STRING_UTILS_HPP

#include <algorithm>
#include <exception>
#include <fstream> // file input
#include <regex>
#include <string>
#include <vector>

using std::fstream;
using std::ifstream;
using std::vector;
using std::string;
using std::exception;
using std::regex;
using std::smatch;
using std::sregex_token_iterator;

//--------------------------------
/* GetMatch( i_source, i_regex, o_match )
	Gets the First match (including capture groups) which matches
	the given regular expression.
*/
static void GetMatch( string const& i_source, string const& i_regex, vector<string>& o_match )
{
	regex regEx(i_regex);
	smatch match;
	if( regex_search(i_source,match,regEx) == true )
	{
		size_t const matchSize = match.size();
		if( matchSize > 1 ) // we had at least one capture in effect
		{
			auto i = match.begin(), e = match.end();
			++i; // skip the first entry (its the full string ignoring captures)
			do
			{
				o_match.push_back( *i );
				++i;
			}
			while( i != e );
		}
		else if( matchSize == 1 )
		{
			o_match.push_back( *match.begin() );
		}
	}
}

//--------------------------------
/* Tokenize( i_src, o_res, i_regexStr )
	Split a string on a regular expression as a token
*/
static void Tokenize( const string& i_src, string const& i_regexStr, vector<string>& o_res )
{
	regex regEx(i_regexStr);
	sregex_token_iterator it( i_src.begin(), i_src.end(), regEx , -1 );
	sregex_token_iterator reg_end;

	for( ; it != reg_end; ++it )
	{
		if( it->str().empty() == false )// skip empty strings
		{
			o_res.emplace_back( it->str() );
		}
	}
}

//--------------------------------
/* Remove( io_src, i_regExStr )
	finds all matches to i_regExStr and replaces them with
	an empty string, thus removing.

	NOTE: it may not be very efficient due to the string assignment
*/
static void Remove( string& io_src, string const& i_regexStr )
{
	std::regex regEx(i_regexStr);
	io_src = std::regex_replace(io_src, regEx, "");
}

//--------------------------------
/* StringToLower( o_str )
	Converts all characters in the string to lower case
*/
static void StringToLower( string& o_str )
{
	std::transform( o_str.begin(), o_str.end(), o_str.begin(), ::tolower );
}

//--------------------------------
/* CheckFileExists( i_fileName )
	Asserts a file exists by attempting to open it
*/
static bool CheckFileExists( string const& i_fileName )
{
	try// file io is inherently likely to throw exceptions
	{
		ifstream fileStream;
		fileStream.open( i_fileName );

		if( fileStream.is_open() )
		{
			fileStream.close();
			return true;
		}
	}
	catch( exception const& e )
	{
		std::cout << e.what() << std::endl;
	}
	return false;
}

#endif
