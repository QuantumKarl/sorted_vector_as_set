#if !defined( D_SYS_PLATFORM )
#define D_SYS_PLATFORM

#if defined(__clang__)
	/* Clang/LLVM. */
	static_assert( false );

#elif defined(__ICC) || defined(__INTEL_COMPILER)
	/* Intel ICC/ICPC. */
	static_assert( false );

#elif defined(__GNUC__) || defined(__GNUG__)
	/* GNU GCC/G++. */
	#define D_GNU

#elif defined(__HP_cc) || defined(__HP_aCC)
	/* Hewlett-Packard C/aC++. */
	static_assert( false );

#elif defined(__IBMC__) || defined(__IBMCPP__)
	/* IBM XL C/C++. */
	static_assert( false );

#elif defined(_MSC_VER)
	/* Microsoft Visual Studio. */
	#define D_WIN

#elif defined(__PGI)
	/* Portland Group PGCC/PGCPP. */
	static_assert( false );

#elif defined(__SUNPRO_C) || defined(__SUNPRO_CC)
	/* Oracle Solaris Studio. */
	static_assert( false );

#endif

#endif