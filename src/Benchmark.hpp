#if !defined( D_BENCH_MARK_HPP )
#define D_BENCH_MARK_HPP
#include <sstream>

//--------------------------------
/* bench( i_dataSize, i_loops, o_result )
	benchmark the performance of doubles and ints in one test.

	NOTE: each action is timed separately. i.e. random number generation, insertion and set operations.
*/
void bench( int64_t const i_dataSize, size_t const i_loops, string& o_result )
{
	//---------------- generate random numbers ----------------
	int64_t const minSize = i_dataSize / -2;
	int64_t const maxSize = i_dataSize / 2;
	std::mt19937_64 gen( i_dataSize*i_loops );
	std::uniform_real_distribution<double> distD( 0.0, 1.0 );
	std::uniform_int_distribution<int64_t> distI( minSize, maxSize );
	std::uniform_int_distribution<int64_t> distIsmall( 0, i_dataSize / 3 );

	vector<int64_t> ranInts;
	vector<int64_t> ranIntsSmall;
	vector<double> ranDoubles;

	//---------------- start testing ----------------
	Timer_t tAll;
	Timer_t tInsert;
	Timer_t tOps;
	Timer_t tRan;
	Timer_t::timeDuration_t tInsertSum( 0 );
	Timer_t::timeDuration_t tOpsSum( 0 );
	Timer_t::timeDuration_t tRanSum( 0 );

	tAll.Start();

	cont_t<int64_t> sI1;
	cont_t<int64_t> sI2;
	cont_t<double> sD1;
	cont_t<double> sD2;

	cont_t<int64_t> sItemp1;
	cont_t<int64_t> sItemp2;
	cont_t<double> sDtemp1;
	cont_t<double> sDtemp2;

	for( size_t j = 0; j < i_loops; ++j )
	{
		//---------------- start timing random numbers ----------------
		tRan.Start();
		sI1.clear();
		sI2.clear();
		sD1.clear();
		sD2.clear();
		sItemp1.clear();
		sItemp2.clear();
		sDtemp1.clear();
		sDtemp2.clear();
		ranInts.clear();
		ranDoubles.clear();
		ranIntsSmall.clear();
		for( int64_t i = 0; i < i_dataSize; ++i )
		{
			ranInts.push_back( distI( gen ) );
			ranInts.push_back( distI( gen ) );

			ranDoubles.push_back( distD( gen ) );
			ranDoubles.push_back( distD( gen ) );
		}

		// one per container (sI2, sD2)
		ranIntsSmall.push_back( distIsmall( gen ) );
		ranIntsSmall.push_back( distIsmall( gen ) );

		tRan.End();
		tRan.CalcDuration();
		tRanSum += tRan.GetDuration();
		//---------------- end timer ----------------

		//---------------- start timing insertion ----------------
		tInsert.Start();
	#if defined( D_USE_INSERT ) && ( D_USE_INSERT == 1) && (D_USE_BATCH_INSERT == 0)
		size_t ranIntIndex = 0;
		size_t ranDoubleIndex = 0;

		for( int64_t i = 0, c = i_dataSize - ranIntsSmall[0]; i < c; ++i )
		{
			sI1.insert( ranInts[ranIntIndex] );
			++ranIntIndex;
		}

		for( int64_t i = 0, c = i_dataSize - ranIntsSmall[1]; i < c; ++i )
		{
			sD1.insert( ranDoubles[ranDoubleIndex] );
			++ranDoubleIndex;
		}

		for( int64_t i = 0; i < i_dataSize; ++i )
		{
			sI2.insert( ranInts[ranIntIndex] );
			++ranIntIndex;

			sD2.insert( ranDoubles[ranDoubleIndex] );
			++ranDoubleIndex;
		}
	#elif defined( D_USE_BATCH_INSERT ) && (D_USE_BATCH_INSERT == 1) && (D_USE_INSERT == 0)
		// Batch adding is much faster, avoid above if possible
		sI1.insert( ranInts.begin(), ranInts.begin() + static_cast<size_t>( i_dataSize - ranIntsSmall[0] ) );
		sD1.insert( ranDoubles.begin(), ranDoubles.begin() + static_cast<size_t>( i_dataSize - ranIntsSmall[1] ) );
		sI2.insert( ranInts.begin() + static_cast<size_t>( i_dataSize ), ranInts.end() );
		sD2.insert( ranDoubles.begin() + static_cast<size_t>( i_dataSize ), ranDoubles.end() );
	#else
		static_assert( false, "need to test something" );
	#endif
		tInsert.End();
		tInsert.CalcDuration();
		tInsertSum += tInsert.GetDuration();
		//---------------- end timer ----------------

		//---------------- start set operations timer ----------------
		// perform some set operations and check that they are equal
		tOps.Start();

		// the intersection is equal to: the union - symmetric difference
		sItemp1 = ( sI1 | sI2 ) - ( sI1 ^ sI2 );
		sItemp2 = ( sI1 & sI2 );
		crashOnFalse( sItemp1 <= sItemp2 );

		sDtemp1 = ( sD1 | sD2 ) - ( sD1 ^ sD2 );
		sDtemp2 = ( sD1 & sD2 );
		crashOnFalse( sDtemp1 <= sDtemp2 );

		// the symmetric difference is equal to: the union - intersection
		sItemp1 = ( sI1 | sI2 ) - ( sI1 & sI2 );
		sItemp2 = ( sI1 ^ sI2 );
		crashOnFalse( sItemp1 >= sItemp2 );

		sDtemp1 = ( sD1 | sD2 ) - ( sD1 & sD2 );
		sDtemp2 = ( sD1 ^ sD2 );
		crashOnFalse( sDtemp1 >= sDtemp2 );

		// the symmetric difference is also equal to: (a-b) U (b-a)
		sItemp1 = ( sI1 - sI2 ) | ( sI2 - sI1 );
		sItemp2 = ( sI1 ^ sI2 );
		crashOnFalse( sItemp1 == sItemp2 );

		sDtemp1 = ( sD1 - sD2 ) | ( sD2 - sD1 );
		sDtemp2 = ( sD1 ^ sD2 );
		crashOnFalse( sDtemp1 == sDtemp2 );

		// the union is equal to: symmetric difference | intersection
		sItemp1 = ( sI1 ^ sI2 ) | ( sI1 & sI2 );
		sItemp2 = ( sI1 | sI2 );
		crashOnFalse( sItemp1 >= sItemp2 );

		sDtemp1 = ( sD1 ^ sD2 ) | ( sD1 & sD2 );
		sDtemp2 = ( sD1 | sD2 );
		crashOnFalse( sDtemp1 >= sDtemp2 );

		// the difference is equal to: a - (a & b)
		sItemp1 = sI1 - ( sI1 & sI2 );
		sItemp2 = ( sI1 - sI2 );
		crashOnFalse( sItemp1 <= sItemp2 );

		sDtemp1 = sD1 - ( sD1 & sD2 );
		sDtemp2 = ( sD1 - sD2 );
		crashOnFalse( sDtemp1 <= sDtemp2 );

		tOps.End();
		tOps.CalcDuration();
		tOpsSum += tOps.GetDuration();
		//---------------- end timer ----------------
	}
	tAll.End();
	tAll.CalcDuration();

	// size, ran, insert, ops, total
	std::stringstream ss;
	ss << i_dataSize << ", ";
	ss << tRanSum.count() << ", ";
	ss << tInsertSum.count() << ", ";
	ss << tOpsSum.count() << ", ";
	ss << tAll.GetDuration().count();

	o_result += ss.str();
}

#endif
