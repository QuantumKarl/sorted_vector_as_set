/*	This file is part of sorted_vector_as_set.
	Copyright(C) 2016 by Karl Wesley Hutchinson

	sorted_vector_as_set is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	sorted_vector_as_set is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with sorted_vector_as_set. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_VEC_SET_TESTS_HPP )
#define D_VEC_SET_TESTS_HPP

#include "short_alloc.hpp"
#include <cassert>

#if defined( D_HAVE_EIGEN ) && (D_HAVE_EIGEN == 1)
	#include<Eigen/Dense>
	#include<Eigen/StdVector>
	EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION( Eigen::ArrayXi )
#endif

#if defined( D_HAVE_BOOST ) && (D_HAVE_BOOST == 1)
	#include <boost/pool/pool_alloc.hpp>
#endif

#include <list>
#include <scoped_allocator>

//--------------------------------
/* IteratorTemplatesTest()
	Test code to assert if the template "is_iterator" is
	working correctly.
*/
void IteratorTemplatesTest()
{
	using namespace sys_templates;
	assert( false == is_iterator<int>::value );
	assert( true == is_iterator<vector<int>::iterator>::value );
	assert( true == is_iterator<std::list<int>::iterator>::value );
	assert( true == is_iterator<string::iterator>::value );
	assert( true == is_iterator<char*>::value ); // a raw pointer is also an iterator
}

//--------------------------------
/* ConstructorTest()
	Targeted at calling constructors.
*/
void ConstructorTest()
{
	std::array<size_t, 10> dataNoDup = { 1,2,3,5,4,6,7,9,8,10 };
	std::array<size_t, 10> dataDup = { 1,1,3,3,2,4,4,5,5,10 };

	vec_set<size_t> a;
	vec_set<size_t> aa( a );
	vec_set<size_t> b{ std::allocator<size_t>() };
	vec_set<size_t> c( a, std::allocator<size_t>() );
	vec_set<size_t> d( begin( dataNoDup ), dataNoDup.end() );
	vec_set<size_t> ed( &dataNoDup[0], &dataNoDup[5] );
	vec_set<size_t> e( dataDup.begin(), end( dataDup ) );
	vec_set<size_t> f( dataNoDup.begin(), dataNoDup.end(), std::allocator<size_t>() );
	vec_set<size_t> g( vec_set<size_t>( { dataNoDup[1],dataNoDup[0] } ) );
	vec_set<string> h( vec_set<string>( { "hello","world" } ), std::allocator<string>() );// copy elison
	vec_set<size_t> hh( vec_set<size_t>( { 1 } ) );// copy elison
	a = b;
	b = vec_set<size_t>( { 4,3,1,1,2,2,2, } );
	c = { 10,10,18 };

	vector< int64_t > vecSrc{ 7,9,11,2,3,5 };
	vector< int64_t > vecA( vecSrc );
	vector< int64_t > vecB( vecSrc );

	vec_set< int64_t > cpyVec( vecA );
	vec_set< int64_t > movVec( move( vecA ) );
	assert( cpyVec == movVec );

	vec_set< int64_t > assVec;
	vec_set< int64_t > movassVec;

	assVec = vecB;
	movassVec = move( vecB );
	assert( assVec == movassVec );
}

//--------------------------------
/* MembersTest()
	Targets calling other functions such as,
	find, shrink_to_fit, size, empty, at, front etc
*/
void MembersTest()
{
	vec_set<size_t> a{ 1,2,3,4,5,6,7,8,9,10,11,12,14 };
	a.pop_back();
	a.reserve( 1024 );
	std::cout << a.capacity() << std::endl;
	std::cout << a.has_unused_capacity() << std::endl;
	std::cout << a.unused_capacity() << std::endl;

	a.insert( 0 );
	a.insert( 5 );
	a.insert( 15 );
	std::cout << a;

	a.shrink_to_fit();
	std::cout << a.size() << std::endl;
	std::cout << a.max_size() << std::endl;
	std::cout << a.empty() << std::endl;
	std::cout << ( a.at( 0 ) + a[a.size() - 1] ) << std::endl;
	std::cout << ( a.front() + a.back() ) << std::endl;

	vec_set<size_t> b{ 1,2,4 };
	b.insert( begin( a ), begin( a ) + 6 );
	b.insert( { 77,5,4,22 } );
	b.erase( 0 );
	b.erase( b.find( 77 ) );
	b.erase( b.find( 2 ), b.find( 7 ) );
	b.swap( a );

	size_t sum = 0;
	for( size_t i = 0; i < b.size(); ++i )
	{
		sum += b[i];
		b[i] = 0;

		sum -= a.at( i );
		a._push_back( sum );
	}
	b._front() = 0;
	b._back() = 8;
	b._at( 2 ) = 10;
	b._resize( 5 );
	b._resize( 10 );
	b.clear();
	vec_set<std::pair<size_t, bool>> thing;
	thing._emplace_back( 5, false );

	vec_set<string> strSet;
	strSet._push_back( string( "HI" ) );
}

//--------------------------------
/* vLessThan
	class used for comparing vectors of int64_t.
*/
class vLessThan
{
public:

	bool operator()( vector<int64_t> const& i_l, vector<int64_t> const& i_r ) const
	{
		auto const lSum = accumulate( begin( i_l ), end( i_l ), static_cast<int64_t>( 0 ) );
		auto const rSum = accumulate( i_r.begin(), i_r.end(), static_cast<int64_t>( 0 ) );
		return lSum < rSum;
	}
};

//--------------------------------
/* vEqual
	class used for comparing vectors of int64_t.
*/
class vEqual
{
public:

	bool operator()( vector<int64_t> const& i_l, vector<int64_t> const& i_r ) const
	{
		auto const lSum = accumulate( i_l.cbegin(), i_l.cend(), static_cast<int64_t>( 0 ) );
		auto const rSum = accumulate( i_r.begin(), i_r.end(), static_cast<int64_t>( 0 ) );
		return lSum == rSum;
	}
};

//--------------------------------
/* vecSet_lessThan
	class used for comparing vec_sets of strings.
*/
class vecSet_lessThan
{
public:
	bool operator()( vec_set<string> const& i_l, vec_set<string> const& i_r ) const
	{
		size_t lSum = 0;
		for( auto& e : i_l )
		{
			lSum += e.size();
		}

		size_t rSum = 0;
		for( auto& e : i_r )
		{
			rSum += e.size();
		}

		return lSum < rSum;
	}
};

#if defined( D_HAVE_EIGEN ) && (D_HAVE_EIGEN == 1)
//--------------------------------
/* arrayXiLess
	class for comparing Eigen::ArrayXi.
*/
class arrayXiLess
{
public:
	bool operator()( Eigen::ArrayXi const& i_l, Eigen::ArrayXi const& i_r ) const
	{
		auto const lMean = i_l.mean();
		auto const rMean = i_r.mean();
		return lMean < rMean;
	}
};

//--------------------------------
/* arrayXiEqual
	class for comparing Eigen::ArrayXi.
*/
class arrayXiEqual
{
public:
	bool operator()( Eigen::ArrayXi const& i_l, Eigen::ArrayXi const& i_r ) const
	{
		return ( i_l == i_r ).all();
	}
};
#endif

//--------------------------------
template <class T, std::size_t BufSize = sizeof( T ) * 64>
using StackVectorSet = vec_set<T, std::less<T>, std::equal_to<T>, short_alloc<T, BufSize, alignof( T )>>;

//--------------------------------
/* NonPodTest()
	targeted at non-pod types.
*/
void NonPodTest()
{
	{
		vec_set< vector<int64_t>, vLessThan, vEqual > vvset;
		vvset.insert( vector<int64_t>( { 1,2,3,4 } ) );
		vvset.insert( vector<int64_t>( { 1,2,3,4 } ) );
		vvset.insert( vector<int64_t>( { 0,2,3,4 } ) );
		vvset.insert( vector<int64_t>( { -4,2,3,4 } ) );
		vec_set< vector<int64_t>, vLessThan, vEqual > vvset2( vvset );
		vvset.erase( vvset.begin() + 1 );
		vvset.erase( vvset.begin() + 1 );
		std::cout << ( vvset == vvset2 ) << std::endl;
	}

	{
		vec_set< vec_set<string>, vecSet_lessThan > vsetvset;
		vsetvset.insert( vec_set<string>( { "Good", "morrow", "fellow", "subjects", "of", "the", "crown" } ) );
		vsetvset.insert( vec_set<string>( { "Hello", "World" } ) );
		vsetvset.insert( vec_set<string>( { "Hello", "World" } ) );
		vsetvset.insert( vec_set<string>( { "World", "Hello" } ) );
		vsetvset.insert( vec_set<string>( { "Yo", "Diggerdy" } ) );
		vsetvset.insert( vec_set<string>( { "Ok", "googol" } ) );
		vec_set< vec_set<string>, vecSet_lessThan > vsetvset2( vsetvset );
		vsetvset.erase( vsetvset.begin() + 1 );
		vsetvset.erase( vsetvset.begin() );
		std::cout << ( vsetvset == vsetvset2 ) << std::endl;
	}

	{
		StackVectorSet<int64_t>::allocator_type::arena_type a;
		StackVectorSet<int64_t>::allocator_type::arena_type b;
		StackVectorSet<int64_t> v1( a );
		v1.insert( { 1,2,3,4,5,3,22,4,2,32,13,-1,6,5,4,3,56,7,33 } );
		v1.erase( { 33,22,1,4,5,3 - 1,34,-10 } );
		StackVectorSet<int64_t> v2( v1, b );
		std::cout << ( v1 != v2 ) << std::endl;
	}

#if defined( D_HAVE_EIGEN ) && (D_HAVE_EIGEN == 1)
	{
		vec_set < Eigen::ArrayXi, arrayXiLess, arrayXiEqual, Eigen::aligned_allocator<Eigen::ArrayXi> > vXi;
		Eigen::ArrayXi temp1( 7 );
		Eigen::ArrayXi temp2( 5 );
		Eigen::ArrayXi temp3( 5 );
		temp1 << 1, 2, 3, 5, -56, 56, 76;
		temp2 << 1, 2, 3, 4, 5;
		temp3 << 5, 4, 3, 2, 1;
		vXi.insert( temp1 );
		vXi.insert( temp2 );
		vXi.insert( temp3 );

		vec_set < Eigen::ArrayXi, arrayXiLess, arrayXiEqual, Eigen::aligned_allocator<Eigen::ArrayXi> > vXi2( vXi );
		vXi.erase( vXi.begin() );
		std::cout << ( vXi == vXi2 ) << std::endl;
	}
#endif

#if defined( D_HAVE_BOOST ) && (D_HAVE_BOOST == 1)
	{
		//--------------------------------
		/* boost source / reference
		http://www.boost.org/doc/libs/1_61_0/libs/pool/doc/html/boost/pool_allocator.html
		..\boost_1_61_0\libs\pool\example
		*/

		typedef boost::pool_allocator<int64_t> alloc_sync;

		vec_set<int64_t, std::less<int64_t>, std::equal_to<int64_t>, alloc_sync> vs1;
		vs1.insert( { 1,2,3,54,32,234,2352,343,55676,86,
			345,35,234,1423,4346,534,234 } );
		vec_set<int64_t, std::less<int64_t>, std::equal_to<int64_t>, alloc_sync> vs2;
		vs1.erase( { 1,2,32 } );
		std::cout << ( vs1 == vs2 ) << std::endl;
	}

	{
		typedef boost::pool_allocator< vector<int64_t> > inner;
		typedef boost::pool_allocator< vec_set< vector<int64_t> > > outer;

		vec_set< vector<int64_t>, vLessThan, vEqual, std::scoped_allocator_adaptor<outer, inner> > vs1;
		vs1._emplace_back( vector<int64_t>{1, 2, 3, 1, -44, 0, 10, 10, 12} );
	}
#endif
}

#endif
