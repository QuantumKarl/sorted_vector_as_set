/*	This file is part of sorted_vector_as_set.
	Copyright(C) 2016 by Karl Wesley Hutchinson

	sorted_vector_as_set is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	sorted_vector_as_set is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with sorted_vector_as_set. If not, see <http://www.gnu.org/licenses/>.
*/

#if !defined( D_WRITE_TO_CSV_HPP )
#define D_WRITE_TO_CSV_HPP

//--------------------------------
/* writeToCSV
	outputs a CSV file.

	NOTE: it expects the data to already be comma separated.
*/
void writeToCSV(vector<string> const& i_data)
{
	//---- Build filename ----
	string fileNameStr;

	//---- add test subject ----
#if defined( D_TEST_VEC_SET ) && (D_TEST_VEC_SET == 1) && (D_TEST_UNORDERD_SET == 0)
	fileNameStr += "vec_set_";
#elif defined( D_TEST_UNORDERD_SET ) && ( D_TEST_UNORDERD_SET == 1) && (D_TEST_VEC_SET == 0)
	fileNameStr += "unordered_set_";
#else
	static_assert(false, "you need to test something");
#endif
	//---- add sub type ----
#if defined( D_USE_INSERT ) && ( D_USE_INSERT == 1) && (D_USE_BATCH_INSERT == 0)
	fileNameStr += "single_insert_looped";
#elif defined( D_USE_BATCH_INSERT ) && (D_USE_BATCH_INSERT == 1) && (D_USE_INSERT == 0)
	fileNameStr += "batch_insert";
#else
	static_assert(false, "you need to test something");
#endif

	try
	{
		//---- create a file handle ----
		std::ofstream outfile(fileNameStr + ".csv"); // default mode overwrites data

													 //---- write the data to file ----
		for (auto& e : i_data)
		{
			outfile << e << '\n';
		}

		outfile.flush();
		outfile.close();
	}
	catch (exception& e)
	{
		std::cout << "FAILED to write output file with exception: \n" << e.what() << std::endl;
	}
}

#endif
