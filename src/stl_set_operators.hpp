/*	This file is part of sorted_vector_as_set.
	Copyright(C) 2016 by Karl Wesley Hutchinson

	sorted_vector_as_set is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	sorted_vector_as_set is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with sorted_vector_as_set. If not, see <http://www.gnu.org/licenses/>.
*/

#if !defined( D_STL_SET_OPERATORS_HPP )
#define D_STL_SET_OPERATORS_HPP

#include "sys_global.hpp"

#include <unordered_set>

//-------------------------------- overloaded operators --------------------------------
/*
	These operators are to provide easy to use set operations:
		| and |= are Union
		- and -= are Difference / subtraction / compliment
		& and &= are Intersection
		^ and ^= are Symmetric difference
*/

//--------------------------------
/* operator|=
	union / merge
	add both sets together, while
	avoiding duplicates in the result

	i.e. a U b

	NOTE: a U a == a
	NOTE: {empty} U a == a
	NOTE: a U {empty} == a
*/
template< typename k, typename h, typename e, typename a>
std::unordered_set<k,h,e,a>& operator|=( std::unordered_set<k,h,e,a>& i_lhs, std::unordered_set<k, h, e, a> const& i_rhs)
{
	if (&i_lhs == &i_rhs || i_rhs.empty() )
	{
		return i_lhs;
	}
	else if (i_lhs.empty())
	{
		i_lhs = i_rhs;
		return i_lhs;
	}
	
	// NOTE: the smaller i_rhs is the better
	i_lhs.insert(i_rhs.begin(), i_rhs.end());

	return i_lhs;
}

//----------------
/* operator|
	same as above
*/
template< typename k, typename h, typename e, typename a>
std::unordered_set<k,h,e,a> operator|( std::unordered_set<k,h,e,a> const& i_lhs, std::unordered_set<k,h,e,a> const& i_rhs)
{
	if (&i_lhs == &i_rhs || i_rhs.empty() )
	{
		return std::unordered_set<k,h,e,a>(i_lhs);
	}
	else if (i_lhs.empty())
	{
		return std::unordered_set<k,h,e,a>(i_rhs);
	}

	auto ptrs = sizeOrder(i_lhs, i_rhs);

	std::unordered_set<k,h,e,a> copy(*ptrs.second); // copy the larger one
	copy |= *ptrs.first; // add the smaller one
	return copy;
}

//--------------------------------
/* operator-=
	subtraction / compliment / difference
	i.e. a - (a U b)

	NOTE: a - a == {empty}
	NOTE: {empty} - a == {empty}
	NOTE: a - {empty} == a
*/
template< typename k, typename h, typename e, typename a>
std::unordered_set<k,h,e,a>& operator-=( std::unordered_set<k,h,e,a>& i_lhs, std::unordered_set<k,h,e,a> const& i_rhs)
{
	if (&i_lhs == &i_rhs)
	{
		i_lhs.clear();
		return i_lhs;
	}
	else if ( i_lhs.empty() || i_rhs.empty() )
	{
		return i_lhs;
	}

	// NOTE: the smaller i_rhs is the better
	for (auto& i : i_rhs)
	{
		i_lhs.erase(i);
	}
	
	return i_lhs;
}

//----------------
/* operator-
	same as above
*/
template< typename k, typename h, typename e, typename a>
std::unordered_set<k,h,e,a> operator-(std::unordered_set<k,h,e,a> const& i_lhs, std::unordered_set<k,h,e,a> const& i_rhs)
{
	if (&i_lhs == &i_rhs)
	{
		return std::unordered_set<k,h,e,a>();
	}
	else if ( i_lhs.empty() || i_rhs.empty() )
	{
		return std::unordered_set<k,h,e,a>(i_lhs);
	}

	// NOTE: order / objects can not be swapped
	std::unordered_set<k,h,e,a> copy(i_lhs);
	copy -= i_rhs;
	return copy;
}

//--------------------------------
/* operator&=
	intersection, the values that are in both sets i.e. a n b

	NOTE: a & a == a
	NOTE: {empty} & a == {empty}
	NOTE: a & {empty} == {empty}
*/
template< typename k, typename h, typename e, typename a>
std::unordered_set<k,h,e,a>& operator&=( std::unordered_set<k,h,e,a>& i_lhs, std::unordered_set<k,h,e,a> const& i_rhs)
{
	if (&i_lhs == &i_rhs)
	{
		return i_lhs;
	}
	else if (i_lhs.empty() || i_rhs.empty())
	{
		i_lhs.clear();
		return i_lhs;
	}

	// NOTE: the smaller i_lhs is the better
	for (auto itr = i_lhs.begin(), endItr = i_lhs.end(); itr != endItr; )
	{
		// try to find ours in theirs
		auto res = i_rhs.find(*itr);

		// if its not found in theirs
		if ( res == i_rhs.end() )
		{
			// remove itr from ours
			auto itrCpy = itr;
			++itr;
			i_lhs.erase(itrCpy);
		}
		else
		{
			// carry on
			++itr;
		}
	}

	return i_lhs;
}

//----------------
/* operator&
	same as above
*/
template< typename k, typename h, typename e, typename a>
std::unordered_set<k,h,e,a> operator&(std::unordered_set<k,h,e,a> const& i_lhs, std::unordered_set<k,h,e,a> const& i_rhs)
{
	if (&i_lhs == &i_rhs)
	{
		return std::unordered_set<k,h,e,a>(i_lhs);
	}
	else if (i_lhs.empty() || i_rhs.empty())
	{
		return std::unordered_set<k,h,e,a>();
	}

	auto ptrs = sizeOrder(i_lhs, i_rhs);

	std::unordered_set<k,h,e,a> copy(*ptrs.first); // take copy of smallest
	copy &= *ptrs.second; // append / search in largest
	return copy;
}

//--------------------------------
/*  operator^=
	symmetric difference a bit like exclusive or, its in either set but NOT in both
	i.e. a ^ b or (a union b) subtract (a intersection b)

	NOTE: a ^ a == {empty}
	NOTE: {empty} ^ a == a
	NOTE: a ^ {empty} == a
*/
template< typename k, typename h, typename e, typename a>
std::unordered_set<k,h,e,a>& operator^=(std::unordered_set<k,h,e,a>& i_lhs, std::unordered_set<k,h,e,a> const& i_rhs)
{
	if (&i_lhs == &i_rhs )
	{
		i_lhs.clear();
		return i_lhs;
	}
	else if (i_lhs.empty())
	{
		i_lhs = i_rhs;
		return i_lhs;
	}
	else if (i_rhs.empty())
	{
		return i_lhs;
	}

	// NOTE: the larger i_lhs it the better
	for( auto& i : i_rhs )
	{
		// try to add theirs to ours
		auto res = i_lhs.insert(i);

		// if it already existed (i.e. it wasn't inserted because it was already there)
		if (res.second == false && res.first != i_lhs.end() )
		{
			// then erase it from ours
			i_lhs.erase(res.first);
		}
	}

	return i_lhs;
}

//----------------
/*  operator^
	same as above
*/
template< typename k, typename h, typename e, typename a>
std::unordered_set<k, h, e, a> operator^(std::unordered_set<k, h, e, a> const& i_lhs, std::unordered_set<k, h, e, a> const& i_rhs)
{
	if (&i_lhs == &i_rhs)
	{
		return std::unordered_set<k, h, e, a>();
	}
	else if (i_lhs.empty())
	{
		return std::unordered_set<k, h, e, a>(i_rhs);
	}
	else if (i_rhs.empty())
	{
		return std::unordered_set<k, h, e, a>(i_lhs);
	}

	auto ptrs = sizeOrder(i_lhs, i_rhs);

	// add theirs to ours, while removing intersections
	std::unordered_set<k, h, e, a> copy(*ptrs.second); // take copy of largest
	copy ^= *ptrs.first; // append smallest

	return copy;
}

//--------------------------------
/* operator<
*/
template< typename k, typename h, typename e, typename a>
bool operator<( std::unordered_set<k, h, e, a> const& i_lhs, std::unordered_set<k, h, e, a> const& i_rhs )
{
	if( i_lhs.size() == i_rhs.size() ) return false;
	return i_lhs <= i_rhs;
}

//--------------------------------
/* operator>
*/
template< typename k, typename h, typename e, typename a>
bool operator>( std::unordered_set<k, h, e, a> const& i_lhs, std::unordered_set<k, h, e, a> const& i_rhs )
{
	if( i_lhs.size() == i_rhs.size() ) return false;
	return i_rhs <= i_lhs;
}

//--------------------------------
/* operator<=
*/
template< typename k, typename h, typename e, typename a>
bool operator<=( std::unordered_set<k, h, e, a> const& i_lhs, std::unordered_set<k, h, e, a> const& i_rhs )
{
	// check if they point to the same object
	if( &i_lhs == &i_rhs ) return true;

	// both are empty
	if( i_lhs.empty() && i_rhs.empty() ) return true;

	// empty is a sub-set of any set
	if( i_lhs.empty() ) return true;

	// check sizes (if left is bigger then right, it can't be a sub-set)
	if( i_lhs.size() > i_rhs.size() ) return false;

	auto itrL = i_lhs.cbegin();
	auto itrLEnd = i_lhs.cend();
	auto itrREnd = i_rhs.cend();
	do// O(n)
	{
		// find left in right
		auto res = i_rhs.find( *itrL ); // O(1)
									// if the left is not found in the right
		if( res == itrREnd )
		{
			return false;
		}
	}
	while( ++itrL != itrLEnd );

	return true;
}

//--------------------------------
/* operator>=
*/
template< typename k, typename h, typename e, typename a>
bool operator>=( std::unordered_set<k, h, e, a> const& i_lhs, std::unordered_set<k, h, e, a> const& i_rhs )
{
	return i_rhs <= i_lhs;
}

//--------------------------------
/* operator>>
*/
template< typename k, typename h, typename e, typename a>
std::ostream& operator<< (std::ostream & o_stream, std::unordered_set<k, h, e, a> const& i_map)
{
	o_stream << '[';
	auto count = i_map.size();
	if (count > 0)
	{
		vector<k> temp;
		temp.reserve(i_map.size());

		for (auto& i : i_map)
		{
			temp.push_back(i);
		}

		std::sort(temp.begin(), temp.end());

		std::cout << temp.front();

		if (count > 1)
		{
			auto cur = temp.begin();
			auto end = temp.end();
			++cur; // we have already done the first element (above)
			do
			{
				o_stream << ',' << *cur;
			} while (++cur != end);
		}
	}
	o_stream << ']' << std::endl;
	return o_stream;
}

//--------------------------------
/* operator>>
*/
template< typename k, typename h, typename e, typename a>
std::istream& operator >> ( std::istream & i_stream, std::unordered_set<k, h, e, a>& o_map )
{
	//-------- setup source --------
	std::string source;
	i_stream >> source;

	//-------- split the string --------
	std::vector<string> split;
	Tokenize( source, ",", split );

	//-------- try extract each value --------
	std::stringstream ss;
	for( auto& element : split )
	{
		k const val = type_to_type<std::string, k>( element, ss );
		o_map.insert( val );
	}
	return i_stream;
}

#endif
