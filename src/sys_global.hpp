#if !defined( D_SYS_GLOBAL_HPP )
#define D_SYS_GLOBAL_HPP 
#include <iostream>
#include <sstream>
#include <string>

//--------------------------------
/* sizeOrder( i_lhs, i_rhs )
	create a pair where first is smaller then second

	NOTE: assumes that the T type has a function called size().
	NOTE: intended to be used with STL containers.
*/
template< typename T >
std::pair<T const* const, T const* const> sizeOrder(T const& i_lhs, T const& i_rhs)
{
	if (i_lhs.size() < i_rhs.size())
	{
		return std::make_pair< T const* const, T const* const >(&i_lhs, &i_rhs);
	}
	else
	{
		return std::make_pair< T const* const, T const* const >(&i_rhs, &i_lhs);
	}
}

//--------------------------------
/* type_to_type( i_type, io_ss )
	This function tries to convert the i_type to
	an o_type via a stringstream. It also clears the
	stringstream, it is ready to be used again.

	NOTE: can throw exceptions.
	NOTE: i_type needs an stream operator <<
	NOTE: o_type needs an stream operator >>
*/
template< typename input_type, typename output_type >
inline
output_type
type_to_type( input_type const& i_type, std::stringstream& io_ss )
{
	io_ss << i_type;
	output_type temp;
	io_ss >> temp;
	io_ss.str( std::string( "" ) );
	io_ss.clear();
	return temp; // hopefully RVO
}

//--------------------------------
/* crashOnFalse( i_val )
	As the name suggests, this function
	causes the program to crash when its passed false.
*/
void crashOnFalse( bool const i_val )
{
	if( i_val == true )
	{
		return;
	}
	else
	{
		int c = *( int* )nullptr;
		std::cout << c << std::endl;
	}
}


#endif
