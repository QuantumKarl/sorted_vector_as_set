#if !defined( D_FUNCTIONALITY_TESTS_HPP )
#define D_FUNCTIONALITY_TESTS_HPP
#include <string>

//--------------------------------
/* CoutFunctionality()
	Targeted at checking functionality.

	NOTE: intended to be used with breakpoints.
*/
void CoutFunctionality()
{
	// test comparison operator
#if 1
	std::default_random_engine generator;
	std::uniform_int_distribution<int64_t> distribution( 0, 24 );
	{
		class more
		{
		public:

			bool operator()( int64_t const a, int64_t const b ) const
			{
				return a > b;
			}
		};

		vec_set<int64_t> dSet1;
		vec_set<int64_t, more> dSet2;

		for( size_t i = 0; i < 24; ++i )
		{
			dSet1.insert( distribution( generator ) );
			dSet2.insert( distribution( generator ) );
		}

		for( auto itr = dSet1._rbegin(), endItr = dSet1._rend(); itr != endItr; ++itr )
		{
			std::cout << *itr << ", ";
			*itr = 0;
		}
		std::cout << std::endl;
		for( auto itr = dSet2.rbegin(), endItr = dSet2.crend(); itr != endItr; ++itr )
		{
			std::cout << *itr << ", ";
		}
		std::cout << std::endl;
	}
#endif

	cont_t<int64_t> const a = { 10,1,1,2,2,3,0,3,4,4,9,9 };
	cont_t<int64_t> const b = { 10,3,3,4,4,5,0,5,6,6,7,7,8,8 };
#if 1
	// test union (operator |,|=)
	{
		cont_t<int64_t> c;
		c = a; // use C as temp so a and b can be reused

		c |= b;
		std::cout << c << std::endl;
		c.clear();

		c = a | b;
		std::cout << c << std::endl;
		c.clear();
		c = b | a;
		std::cout << c << std::endl;
	}
#endif

	// subtraction / difference (operator -,-=)
#if 1
	{
		cont_t<int64_t> c;
		c = a; // use C as temp so a and b can be reused

		c -= b;
		std::cout << c << std::endl;
		c.clear();

		c = a - b;
		std::cout << c << std::endl;
		c.clear();
		c = b;
		c -= a;
		std::cout << c << std::endl;
		c.clear();

		c = b - a;
		std::cout << c << std::endl;
	}
#endif

#if 1
	// test intersection (operator &,&=)
	{
		cont_t<int64_t> c;
		c = a; // use C as temp so a and b can be reused

		c &= b;
		std::cout << c << std::endl;
		c.clear();

		c = a & b;
		std::cout << c << std::endl;
		c.clear();
		c = b & a;
		std::cout << c << std::endl;
	}
#endif

#if 1
	{ // symmetric difference
		cont_t<int64_t> c;
		c = a; // use C as temp so a and b can be reused

		c ^= b;
		std::cout << c << std::endl;
		c.clear();

		c = a ^ b;
		std::cout << c << std::endl;
		c.clear();
		c = b ^ a;
		std::cout << c << std::endl;

		c.clear();
		c = ( a | b ) - ( a & b );
		std::cout << c << std::endl;
	}
#endif

#if 1
	// test comparison (operator ==,!=)
	{
		cont_t<int64_t> c;
		c = a; // use C as temp so a and b can be reused
		std::cout << ( c == a ) << std::endl;
		c = b;
		std::cout << ( c == b ) << std::endl;
		std::cout << ( c != b ) << std::endl;
	}
#endif

#if 1
	{
		std::cout << (a <= b) << std::endl;
		std::cout << ( (a&b) <= a ) << std::endl;
	}
#endif

#if 1
	{
		std::cout << (a < a) << std::endl;
		std::cout << ( (a&b) < a ) << std::endl;
	}
#endif
}

//--------------------------------
/* detail_functionality_tests
	Namespace containing helper functions for the ComparedFunctionaltyTest().
*/
namespace detail_functionality_tests
{
	//--------------------------------
	/* MatchToList( i_leadingExp, io_source, o_list )
		Given a leading expression eg "A:" this function picks out the list from the source.
		eg source could look like "A: [2,3,5,7,11]" this would be capture the list and return "2,3,5,7,11"
	*/
	bool MatchToList( string const i_leadingExp, string& io_source, string& o_list )
	{
		vector<string> matches;
		Remove( io_source, "\\s" );
		GetMatch( io_source, i_leadingExp + "\\[([\\d\\,]+)", matches );

		if( matches.size() == 1 )
		{
			o_list = matches.front();
			return true;
		}
		else
		{
			o_list.clear();
			return false;
		}
	}

	//--------------------------------
	/* BoolInStringToBool( i_leadingExp, io_source, o_value )
		Given a leading expression (much like above) and a source looking something like
		"A<=B: True" this function will return true.
	*/
	bool BoolInStringToBool( string const i_leadingExp, string& io_source, bool& o_value )
	{
		vector<string> matches;
		Remove( io_source, "\\s" );
		GetMatch( io_source, i_leadingExp + "(\\w+)", matches );

		if( matches.size() == 1 )
		{
			auto& str = matches.front();
			StringToLower( str );
			if( str == "true" )
			{
				o_value = true;
			}
			else if( str == "false" )
			{
				o_value = false;
			}
			else //else is some other string
			{
				return false;
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	//--------------------------------
	/* c_leadingExpressions
		These values match the strings to be output inside of SetOps.py.
		They are used to match against the results from SetOps.py
	*/
	std::array<string, 16> const c_leadingExpressions =
	{
		"conda activate",
		"A:",
		"B:",
		"A\\|B:",
		"A\\&B:",
		"A\\-B:",
		"B\\-A:",
		"A\\^B:",
		// the following return booleans
		"a<=b:",
		"a>=b:",
		"\\(a\\&b\\)<=a:",
		"\\(a\\|b\\)>=a:",
		"a<b:",
		"a>b:",
		"\\(a\\&b\\)<a:",
		"\\(a\\|b\\)>a:"
	};

}// end of namespace

//--------------------------------
/* ComparedFunctionalityTest( i_setSize, i_setSizeRatio, i_seed, i_numTests, i_debugCout )
	This function calls a python script. The python script then generates sets and performs set operations on them.
	After that, this function continues to parse the results, then compares the results from python with our results.

	NOTE: This function is rather slow due to using the exec function (it runs commands in the system terminal)
*/
void ComparedFunctionalityTest
(
	size_t const i_setSize,			// the max size of the set
	double const i_setSizeRatio,	// the ratio difference in the sizes of the sets
	size_t const i_dataRange,		// the max value in the set rte [0,i_dataRange]
	size_t const i_seed,			// the seed value for the PRNG in python
	size_t const i_numTests,		// the number of tests to be ran (it automatically updates seed for each iteration)
	bool const i_debugCout = false	// if set to true, information is cout-ed
)
{
	using namespace detail_functionality_tests;

	//-------- check we have the python script --------
	if( CheckFileExists("SetOps.py") == false )
	{
		std::cout << "ERROR: SetOps.py is not in the same directory as the executable, unable to compare functionality." << std::endl;
		return;
	}

	//-------- setup args --------
	std::stringstream ss;

	std::string const setSize = type_to_type<std::size_t,std::string>( i_setSize, ss );
	std::string const setSizeRatio = type_to_type<double,std::string>( i_setSizeRatio, ss );
	std::string const dataRange = type_to_type<std::size_t,std::string>( i_dataRange, ss );

	std::string seed;
	for( size_t numTests = 0; numTests < i_numTests; ++numTests )
	{
		//-------- setup seed --------
		seed.clear();
		seed = type_to_type<std::size_t,std::string>( i_seed + numTests, ss );

		//-------- create command --------
		std::string const cmd = "activate && python SetOps.py " + setSize + " " + setSizeRatio + " " + dataRange + " " + seed;
		std::string output = "";

		//-------- execute command --------
		if( exec( cmd, output ) == false )
		{
			std::cout << "Error calling SetOps.py" << std::endl;
			return;
		}

		//-------- split the single line into multiple lines --------
		std::vector<std::string> lines;
		Tokenize( output, "\\n|$", lines );

		//-------- setup storage --------
		std::vector< cont_t<int64_t > > outputSets;
		std::vector< bool > outputBools;

		//-------- quick check we are valid --------
		constexpr size_t numBoolAnswers = 8;
		assert( lines.size() >= numBoolAnswers );
		{
			//-------- create temp --------
			std::string tempStr;

			//-------- extract sets from output --------
			for( size_t i = 1, c = lines.size() - numBoolAnswers; i < c; ++i )
			{
				MatchToList( c_leadingExpressions[i], lines[i], tempStr );
				outputSets.emplace_back();
				outputSets.back() = type_to_type< std::string, cont_t<int64_t> >( tempStr, ss );
				tempStr.clear();
			}
		}
		//-------- extract bools from output --------
		for( size_t i = lines.size() - numBoolAnswers, c = lines.size(); i < c; ++i )
		{
			bool refBool = false;
			BoolInStringToBool( c_leadingExpressions[i], lines[i], refBool );
			outputBools.emplace_back( refBool );
		}

		//-------- create easy to read references --------
		auto& a = outputSets[0];
		auto& b = outputSets[1];
		auto& a_union_b = outputSets[2];
		auto& a_intersection_b = outputSets[3];
		auto& a_subtract_b = outputSets[4];
		auto& b_subtract_a = outputSets[5];
		auto& a_symmetric_difference_b = outputSets[6];
		auto a_subset_b = outputBools[0];
		auto a_superset_b = outputBools[1];
		auto a_intersection_b_subset_a = outputBools[2];
		auto a_union_b_superset_a = outputBools[3];
		auto a_proper_subset_b = outputBools[4];
		auto a_proper_superset_b = outputBools[5];
		auto a_proper_intersection_b_subset_a = outputBools[6];
		auto a_proper_union_b_superset_a = outputBools[7];

		//-------- cout if requested --------
		if( i_debugCout == true )
		{
			std::cout << output << "\n--------------------------------\n";
		}

		//-------- check container output vs python output --------
		crashOnFalse( a_union_b == ( a | b ) );
		crashOnFalse( a_intersection_b == ( a & b ) );
		crashOnFalse( a_subtract_b == ( a - b ) );
		crashOnFalse( b_subtract_a == ( b - a ) );
		crashOnFalse( a_symmetric_difference_b == ( a ^ b ) );
		crashOnFalse( a_subset_b == ( a <= b ) );
		crashOnFalse( a_superset_b == (a >= b ) );
		crashOnFalse( a_intersection_b_subset_a == ( a&b ) <= a );
		crashOnFalse( a_union_b_superset_a == ( ( a | b ) >= a ) );
		crashOnFalse( a_proper_subset_b == ( a < b ) );
		crashOnFalse( a_proper_superset_b == ( a > b ) );
		crashOnFalse( a_proper_intersection_b_subset_a == ( ( a&b ) < a ) );
		crashOnFalse( a_proper_union_b_superset_a == ( ( a | b ) > a ) );
	}
}

#endif
