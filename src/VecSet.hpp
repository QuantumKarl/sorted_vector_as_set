﻿/*	This file is part of sorted_vector_as_set.
	Copyright(C) 2016 by Karl Wesley Hutchinson

	sorted_vector_as_set is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	sorted_vector_as_set is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with sorted_vector_as_set. If not, see <http://www.gnu.org/licenses/>.
*/

#if !defined( D_VEC_SET_HPP )
#define D_VEC_SET_HPP

#include "sys_global.hpp"
#include "sys_stringUtils.hpp"

#include <algorithm>
#include <assert.h>
#include <iostream>
#include <istream>
#include <iterator>
#include <utility>
#include <vector>

namespace sys_templates
{
	using namespace std;
	// TEMPLATE STRUCT _Param_tester
	template<class...>
	struct param_tester
	{	// test if parameters are valid
		typedef void type;
	};

	// ALIAS TEMPLATE void_t
	template<class... _Types>	// TRANSITION, needs CWG 1558
	using void_t = typename param_tester<_Types...>::type;

	// TEMPLATE CLASS is_iterator
	template<class T, class = void>
	struct is_iterator
		:
		false_type
	{	// default definition
	};

	template<class T>
	struct is_iterator
	<
		T,
		void_t
		<
			typename iterator_traits<T>::iterator_category
		>
	>
		:
		true_type
	{	// defined if iterator_category is provided by iterator_traits<T>
	};
}

//-------------------------------- vec_set --------------------------------
/* vec_set - vector based set

	What is it:
	Rather similar to std::unordered_set however without wasting large amounts of memory (flat,vector).

	The contents have the following properties:
		data is sorted - its order depends on what comparison operator is given (less / ascending) is default.
		no duplicate values - this in not a multi-set, only one of each value is permitted.

	What can it do:
	This class contains a set of elements, its main purpose is the use of
	overloaded operators for convenient use of set operations:
		union:				O( 2(n+m)-1 ), | and |=
		subtraction: 		O( 2(n+m)-1 ), - and -=
		intersection:		O( 2(n+m)-1 ), & and &=
		symmetric difference:	O( 2(n+m)-1 ) ^ and ^=
		proper sub-set:		O( 2(n+m)-1 ), <
		sub-set:			O( 2(n+m)-1 ), <=
		proper super-set:	O( 2(n+m)-1 ), >
		super-set:			O( 2(n+m)-1 ), >=
		equality:			O(n), ==
		inequality:		O(n), !=
			where:
			n = lhs.size()
			m = rhs.size()
			with equality and inequality n == m

	How it works:
	It uses std::lower_bound to find values. This is used for inserting and erasing individual values.
	Inserting in batch is more efficient since sorting and removing duplicates is fast.

	Is it fast?
	The find operation takes O(log n) comparisons. This is used in erase(val), insert(val).
	The set operations are near O(n) in complexity. Their documented complexity is O( 2(n+m)-1 ) where n and m are the size of each container involved.

	How to use it efficiently?
	Batch insert,
	then do as many set operations you like.
	Avoid frequent insert(val), erase(val) and find(val).

	Read http://lafstern.org/matt/col1.pdf for motivation of using a vector as a container for a set.
*/

template < typename T, typename compare_t = std::less<T>, typename equal_t = std::equal_to<T>, typename allocator_t = std::allocator<T>  >
class vec_set final
{
public:
	/*---------------- aliases --------------------------------
		these aliases are to mimic stl and to make the
		code shorter / easier to read.
	*/

	// vec_set allocator
	using allocator_type = allocator_t;

	// vec_set container
	using cont_t = vector<T, allocator_type> ;

	// an external version of vec_set's container (with templated allocator)
	template< typename alloc_t >
	using external_cont_t = vector<T, alloc_t>;

	// types
	using value_type		= typename cont_t::value_type;
	using size_type			= typename cont_t::size_type;
	using difference_type	= typename cont_t::difference_type;

	// pointers
	using pointer			= typename cont_t::pointer;
	using const_pointer		= typename cont_t::const_pointer;
	using reference			= typename cont_t::reference;
	using const_reference	= typename cont_t::const_reference;

	// iterators
	using iterator			= typename cont_t::iterator;
	using const_iterator	= typename cont_t::const_iterator;
	using reverse_iterator	= typename cont_t::reverse_iterator;
	using const_reverse_iterator	= typename cont_t::const_reverse_iterator;

	// vec_set class type
	using this_type = vec_set<value_type, compare_t, equal_t, allocator_type>;

	//---------------- constructors --------------------------------

	//--------------------------------
	/* ctor()
		default ctor
	*/
	vec_set() noexcept
		:
		m_v(),
		m_cmp(),
		m_eql()
	{ }

	//--------------------------------
	/* ctor( allocator )
		ctor with allocator
	*/
	explicit vec_set( allocator_type const& i_allocator ) noexcept
		:
		m_v( i_allocator ),
		m_cmp(),
		m_eql()
	{ }

	//--------------------------------
	/* ctor( this_type const& )
		construct by copying i_rhs
	*/
	vec_set( this_type const& i_rhs ) noexcept
		:
		m_v( i_rhs.m_v ),
		m_cmp( i_rhs.m_cmp ),
		m_eql( i_rhs.m_eql )
	{ }

	//--------------------------------
	/* ctor( this_type const&, allocator )
		construct by copying i_rhs, allocator
	*/
	vec_set( this_type const& i_rhs, allocator_type const& i_allocator ) noexcept
		:
		m_v( i_rhs.m_v, i_allocator ),
		m_cmp( i_rhs.m_cmp ),
		m_eql( i_rhs.m_eql )
	{ }

	//--------------------------------
	/* ctor( external_cont_t const& )
		construct from a matching container

		NOTE: since vector is used internally, vector< value_t, alloc_t > matches this.
	*/
	template< typename alloc_t >
	vec_set( external_cont_t< alloc_t > const& i_cont )
		:
		m_v( i_cont ), // take a copy
		m_cmp(),
		m_eql()
	{
		validate();
	}

	//--------------------------------
	/* ctor( external_cont_t&& )
		construct from a matching container

		NOTE: since vector is used internally, vector< value_t, alloc_t > matches this.
		NOTE: if you know i_cont is a valid set (and sorted),
		you can use _move_external_cont_to_internal_cont to directly move into the internal container.
	*/
	template< typename alloc_t >
	vec_set( external_cont_t< alloc_t >&& i_cont )
		:
		m_v( move(i_cont) ), // move the cont
		m_cmp(),
		m_eql()
	{
		validate();
	}

	//--------------------------------
	/* ctor( first, last )
		construct from [i_first, i_last)
	*/
	template<class itr_t, class = typename std::enable_if<sys_templates::is_iterator<itr_t> ::value, void>::type>
	vec_set( itr_t i_first, itr_t i_last )
		:
		m_v(),
		m_cmp(),
		m_eql()
	{
		insert( i_first, i_last );
	}

	//--------------------------------
	/* ctor( first, last, allocator )
		construct from [i_first, i_last) with allocator
	*/
	template<class itr_t, class = typename std::enable_if<sys_templates::is_iterator<itr_t> ::value, void>::type>
	vec_set( itr_t i_first, itr_t i_last, allocator_type const& i_allocator )
		:
		m_v( i_allocator ),
		m_cmp(),
		m_eql()
	{
		insert( i_first, i_last );
	}

	//--------------------------------
	/* ctor( this_type&& )
		construct by moving i_rhs
	*/
	vec_set( this_type&& i_rhs ) noexcept
		:
		m_v( move( i_rhs.m_v ) ),
		m_cmp( move( i_rhs.m_cmp ) ),
		m_eql( move( i_rhs.m_eql ) )
	{ }

	//--------------------------------
	/* ctor( this_type&&, allocator )
		construct by moving i_rhs, allocator
	*/
	vec_set( this_type&& i_rhs, allocator_type const& i_allocator ) noexcept
		:
		m_v( move( i_rhs.m_v ), i_allocator ),
		m_cmp( move( i_rhs.m_cmp ) ),
		m_eql( move( i_rhs.m_eql ) )
	{ }

	//--------------------------------
	/* ctor( {init_list} )
		construct from initializer_list
	*/
	vec_set( std::initializer_list<value_type> i_initList )
		:
		m_v(),
		m_cmp(),
		m_eql()
	{
		insert( i_initList.begin(), i_initList.end() );
	}

	//--------------------------------
	/* ctor( {init_list} )
		construct from initializer_list with allocator
	*/
	vec_set( std::initializer_list<value_type> i_initList, allocator_type const& i_allocator )
		:
		m_v( i_allocator ),
		m_cmp(),
		m_eql()
	{
		insert( i_initList.begin(), i_initList.end() );
	}

	//---------------- assignment operators --------------------------------

	//--------------------------------
	/* operator= this_type const&
		assign right
	*/
	this_type& operator=( this_type const& i_rhs ) noexcept
	{
		m_v = i_rhs.m_v;
		m_cmp = i_rhs.m_cmp;
		m_eql = i_rhs.m_eql;
		return *this;
	}

	//--------------------------------
	/* operator= this_type&&
		assign by moving i_rhs
	*/
	this_type& operator=( this_type&& i_rhs ) noexcept
	{
		m_v = move( i_rhs.m_v );
		m_cmp = move( i_rhs.m_cmp );
		m_eql = move( i_rhs.m_eql );
		return *this;
	}

	//--------------------------------
	/* operator= external_cont_t&
		assign a matching container

		NOTE: since vector is used internally, vector< value_t, alloc_t > matches this.
		NOTE: external_cont_t does not have to be a valid set.
	*/
	template< typename alloc_t >
	this_type& operator=( external_cont_t< alloc_t > const& i_rhs ) noexcept
	{
		m_v = i_rhs; // take a copy
		// m_cmp = whatever this was constructed with
		// m_eql = whatever this was constructed with

		validate();
		return *this;
	}

	//--------------------------------
	/* operator= external_cont_t&&
		assign right

		NOTE: since vector is used internally, vector< value_t, alloc_t > matches this.
		NOTE: external_cont_t does not have to be a valid set.
		NOTE: if you know i_rhs is a valid set (and sorted),
		you can use _move_external_cont_to_internal_cont to directly move into the internal container.
	*/
	template< typename alloc_t >
	this_type& operator=( external_cont_t< alloc_t >&& i_rhs ) noexcept
	{
		m_v = move( i_rhs );
		// m_cmp = whatever this was constructed with
		// m_eql = whatever this was constructed with

		validate();
		return *this;
	}

	//--------------------------------
	/* operator= {init_list}
		assign initializer_list
	*/
	this_type& operator=( std::initializer_list<value_type> i_initList )
	{
		clear();
		insert( i_initList.begin(), i_initList.end(), i_initList.size() );
		return *this;
	}

	//---------------- other member operators --------------------------------

	//--------------------------------
	/* operator[]
		subscript non-mutable sequence
	*/
	const_reference operator[]( size_type const i_index ) const
	{
		return m_v[i_index];
	}

	//--------------------------------
	/* dtor()
		destroy the object / object's fall out of scope

		NOTE: if you have raw pointers in this container,
		they will leak at this point! it doesn't do anything for you
	*/
	~vec_set() noexcept
	{ }

	//---------------- member functions --------------------------------

	//--------------------------------
	/* at( i_index )
		subscript non-mutable sequence with checking
	*/
	const_reference at( size_type const i_index ) const
	{
		return m_v.at( i_index );
	}

	//--------------------------------
	/* back()
		return last element of non mutable sequence
	*/
	const_reference back() const
	{
		return m_v.back();
	}

	//--------------------------------
	/* begin()
		return iterator for beginning of non-mutable sequence
	*/
	inline const_iterator begin() const noexcept
	{
		return m_v.cbegin();
	}


	//--------------------------------
	/* capacity()
		return current length of allocated storage
	*/
	size_type capacity() const noexcept
	{
		return m_v.capacity();
	}

	//--------------------------------
	/* cbegin()
		return iterator for beginning of non-mutable sequence
	*/
	inline const_iterator cbegin() const noexcept
	{
		return m_v.cbegin();
	}

	//--------------------------------
	/* cend()
		return iterator for end of non mutable sequence
	*/
	inline const_iterator cend() const noexcept
	{
		return m_v.cend();
	}

	//--------------------------------
	/* clear()
		erase all / make empty
	*/
	inline void clear() noexcept
	{
		m_v.clear();
	}

	//--------------------------------
	/* cbegin()
		return iterator for the back of non-mutable sequence.

		NOTE: reverse iterator.
	*/
	inline const_reverse_iterator crbegin() const noexcept
	{
		return m_v.crbegin();
	}

	//--------------------------------
	/* crend()
		return iterator for the front of non-mutable sequence.

		NOTE: reverse iterator.
	*/
	inline const_reverse_iterator crend() const noexcept
	{
		return m_v.crend();
	}

	//--------------------------------
	/* empty()
		ask if sequence is empty
	*/
	inline bool empty() const noexcept
	{
		return m_v.empty();
	}

	//--------------------------------
	/* end()
		return iterator for end of non mutable sequence
	*/
	inline const_iterator end() const noexcept
	{
		return m_v.cend();
	}

	//--------------------------------
	/* erase( value_type const& )
		erase element by value
	*/
	const_iterator erase( value_type const& i_value )
	{
		auto itr = find( i_value );
		return itr != end() ? m_v.erase( itr ) : end();
	}

	//--------------------------------
	/* erase( const_iterator )
		erase element at iterator
	*/
	inline const_iterator erase( const_iterator i_itr )
	{
		return m_v.erase( i_itr );
	}

	//--------------------------------
	/* erase( const_iterator, const_iterator )
		erase [i_first, i_last)
	*/
	const_iterator erase( const_iterator i_first, const_iterator i_last )
	{
		return m_v.erase( i_first, i_last );
	}

	//--------------------------------
	/* erase( {init_list} )
		erase each element in init list
	*/
	void erase( std::initializer_list<value_type> i_initList )
	{
		for( auto& e : i_initList )
		{
			erase( e );
		}
	}

	//--------------------------------
	/* find( value_type const& )
		find value
	*/
	const_iterator find( value_type const& i_val ) const
	{
		auto itr = std::lower_bound( cbegin(), cend(), i_val, m_cmp );
		return itr == cend() || m_cmp( i_val, *itr ) ? cend() : itr;
	}

	//--------------------------------
	/* front()
		return first element of non-mutable sequence
	*/
	const_reference front() const
	{
		return m_v.front();
	}

	//--------------------------------
	/* get_allocator()
		return allocator object for values
	*/
	inline allocator_type get_allocator() const noexcept
	{
		return m_v.get_allocator();
	}

	//--------------------------------
	/* get_compare()
		return the compare object which the data
		is ordered by.
	*/
	inline compare_t const& get_compare() const noexcept
	{
		return m_cmp;
	}

	//--------------------------------
	/* get_equal
		return the compare object which the data
		is defined to be equal.
	*/
	inline equal_t const& get_equal() const noexcept
	{
		return m_eql;
	}

	//--------------------------------
	/*  has_unused_capacity()
		micro-optimization for capacity() != size()
	*/
	bool has_unused_capacity() const noexcept
	{
		return m_v.capacity() != m_v.size();
	}

	//--------------------------------
	/* insert( value_type& i_val )
		insert an element (in the correct sorted position)
	*/
	const_iterator insert( value_type const& i_val )
	{
		auto itr = std::lower_bound( cbegin(), cend(), i_val, m_cmp );
		if( itr == cend() || m_cmp( i_val, *itr ) )
		{
			m_v.insert( itr, i_val );
		}
		return itr;
	}

	//--------------------------------
	/* insert( value_type&& i_val )
		insert an element (in the correct sorted position)
	*/
	const_iterator insert( value_type&& i_val )
	{
		auto itr = std::lower_bound( cbegin(), cend(), i_val, m_cmp );
		if( itr == cend() || m_cmp( i_val, *itr ) )
		{
			m_v.insert( itr, std::move( i_val ) );
		}
		return itr;
	}

	//--------------------------------
	/* insert( i_first, i_last )
		insert multiple elements (all in correct order, duplicates removed)
	*/
	template<class itr_t, class = typename std::enable_if<sys_templates::is_iterator<itr_t> ::value, void>::type>
	inline void insert( itr_t i_first, itr_t i_last )
	{
		// calculate new size
		auto const newSize = size() + std::distance( i_first, i_last );
		// WARNING: Distance O(n) if not random access container / iterator

		reserve( newSize );
		m_v.insert( _end(), i_first, i_last );

		validate();
	}

	//--------------------------------
	/* insert( i_first, i_last, i_size )
		insert multiple elements (all in correct order, duplicates removed)

		NOTE: providing a smaller size will not make it add less elements.
		NOTE: the size parameter is intended as an optimisation, since containers usually keep track of their size.
	*/
	template<class itr_t, class = typename std::enable_if<sys_templates::is_iterator<itr_t> ::value, void>::type>
	inline void insert( itr_t i_first, itr_t i_last, size_t const i_size )
	{
		assert( i_size == static_cast<size_t>( std::distance( i_first, i_last ) ) );

		// calculate new size
		auto const newSize = size() + i_size;

		reserve( newSize );
		m_v.insert( _end(), i_first, i_last );

		validate();
	}

	//--------------------------------
	/* insert( {...} )
		insert a list of elements (all in correct order, duplicates removed)
	*/
	void insert( std::initializer_list<value_type> i_initList )
	{
		insert( i_initList.begin(), i_initList.end(), i_initList.size() );
	}

	//--------------------------------
	/* max_size()
		return maximum possible length of sequence /
		ask the allocator its max size
	*/
	size_type max_size() const noexcept
	{
		return get_allocator().max_size();
	}

	//--------------------------------
	/* pop_back()
		remove element at end
	*/
	void pop_back()
	{
		m_v.pop_back();
	}

	//--------------------------------
	/* rbegin()
		return iterator for the back of non-mutable sequence.

		NOTE: reverse iterator.
	*/
	inline const_reverse_iterator rbegin() const noexcept
	{
		return m_v.rbegin();
	}

	//--------------------------------
	/* rend()
		return iterator for the front of non-mutable sequence.

		NOTE: reverse iterator.
	*/
	inline const_reverse_iterator rend() const noexcept
	{
		return m_v.rend();
	}

	//--------------------------------
	/* reserve( i_count )
		set new minimum length of allocated storage
	*/
	inline void reserve( size_type const i_count )
	{
		m_v.reserve( i_count );
	}

	//--------------------------------
	/* shrink_to_fit()
		reduce capacity to fit the only used size
	*/
	void shrink_to_fit()
	{
		m_v.shrink_to_fit();
	}

	//--------------------------------
	/* size()
		return length of sequence /
		number of inserted items
	*/
	inline size_type size() const noexcept
	{
		return m_v.size();
	}

	//--------------------------------
	/* swap( this_type& )
		exchange contents with right
	*/
	void swap( this_type& i_rhs ) noexcept
	{
		std::swap( m_v, i_rhs.m_v );
		std::swap( m_cmp, i_rhs.m_cmp );
		std::swap( m_eql, i_rhs.m_eql );
	}

	//--------------------------------
	/* unused_capacity()
		micro-optimization for capacity() - size()
	*/
	size_type unused_capacity() const noexcept
	{
		return capacity() - size();
	}

	//--------------------------------
	/* validate()
		if you have manually manipulated the container via
		a function such as _push_back, then calling this function
		will restore the set properties.

		NOTE: even though its big O notion looks horrid,
		its actually fast since both calls don't have much work to do.
	*/
	inline void validate()
	{
		std::sort( _begin(), _end(), m_cmp ); // O(n log n)
		auto newEnd = std::unique( _begin(), _end(), m_eql ); // O(n)
		m_v.resize( std::distance( m_v.begin(), newEnd ) );
	}

	//---------------- functions that compromise set --------------------------------
	/*
		Using these functions damage the internal properties of:
			being a set
			having all values unique
			sorted data

		Therefore, it can crash if your not careful when you are using these
		functions.

		You can make sure everything is correct by calling validate() after using these functions.
		(preferably once after many calls / batch use these functions then call validate() once)
	*/

	//--------------------------------
	/* _at(size_type const)
		subscript mutable sequence with checking
	*/
	reference _at( size_type const i_pos )
	{
		return m_v.at( i_pos );
	}

	//--------------------------------
	/* _begin()
		return iterator for beginning of mutable sequence
	*/
	inline iterator _begin() noexcept
	{
		return m_v.begin();
	}

	//--------------------------------
	/* _back()
		return last element of mutable sequence
	*/
	reference _back()
	{
		return m_v.back();
	}

	//--------------------------------
	/* _emplace_back( vaList_t&&... )
		insert by moving into element at end
	*/
	template<class... vaList_t>
	void _emplace_back( vaList_t&&... i_values )
	{
		m_v.emplace_back( std::forward<vaList_t>( i_values )... );
	}

	//--------------------------------
	/* _end()
		return iterator for end of mutable sequence
	*/
	inline iterator _end() noexcept
	{
		return m_v.end();
	}

	//--------------------------------
	/* _find( value_type const& )
		find element by value and return a none cost
		iterator
	*/
	inline iterator _find( value_type const& i_val )
	{
		auto itr = std::lower_bound( _begin(), _end(), i_val, m_cmp );
		return itr == _end() || m_cmp( i_val, *itr ) ? _end() : itr;
	}

	//--------------------------------
	/* _front()
		return first element of mutable sequence
	*/
	reference _front()
	{
		return m_v.front();
	}

	//--------------------------------
	/* _move_external_cont_to_internal_cont
		directly assign to the internal cont
		by moving i_rhs.
	*/
	template< typename alloc_t >
	inline this_type& _move_external_cont_to_internal_cont( external_cont_t< alloc_t >&& i_rhs ) noexcept
	{
		m_v = std::move( i_rhs );
		return *this;
	}

	//--------------------------------
	/* _push_back( value_type&& )
		insert by moving into element at end
	*/
	void _push_back( value_type&& i_value )
	{
		m_v.push_back( std::move( i_value ) );
	}

	//--------------------------------
	/* _push_back( value_type const& )
		insert by moving into element at end
	*/
	void _push_back( value_type const& i_value )
	{
		m_v.push_back( i_value );
	}

	//--------------------------------
	/* _rbegin()
		return iterator for the back of mutable sequence.

		NOTE: reverse iterator.
	*/
	inline reverse_iterator _rbegin() noexcept
	{
		return m_v.rbegin();
	}

	//--------------------------------
	/* _rend()
		return iterator for the front of mutable sequence.

		NOTE: reverse iterator.
	*/
	inline reverse_iterator _rend() noexcept
	{
		return m_v.rend();
	}

	//--------------------------------
	/* _resize(size_type const)
		set new length, padded as needed

		NOTE: resize only make sense if its shrinking,
		since the padding adds duplicates
	*/
	void _resize( size_type const i_newSize )
	{
		// optional assert, enable if you intend to only shrink using resize
	#if 0
		assert( i_newSize < size() );
	#endif
		m_v.resize( i_newSize );
	}

	//--------------------------------
	/* operator[]
		subscript mutable sequence
	*/
	reference operator[]( size_type const i_pos )
	{
		return m_v[i_pos];
	}

private:
	//---------------- private data members ----------------
	cont_t m_v;
	compare_t m_cmp;
	equal_t m_eql;
};

//-------------------------------- overloaded operators --------------------------------
/*
	These operators are to provide easy to use set operations:
		| and |= are Union
		- and -= are Difference / subtraction / compliment
		& and &= are Intersection
		^ and ^= are Symmetric difference
*/

//--------------------------------
/* detail_vec_set_operators
	These functions are common to some of the operators.
	They are not intended to be seen or used by end users,
	however, there is nothing stopping them accessing the namespace.
*/
namespace detail_vec_set_operators
{
	//-------------------------------- functions for |,|=,-,-=,&,&=,^ and ^= operators --------------------------------

	//--------------------------------
	/* set_union( i_lhs, i_rhs )
		O( 2(n+m)-1 )
			where:
				n = i_lhs.size()
				m = i_rhs.size()

		NOTE: used in |, and |=
	*/
	template< typename t, typename c, typename e, typename a>
	inline
	std::vector<t, a>
	set_union( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
	{
		assert( i_lhs.empty() == false && i_rhs.empty() == false );

		std::vector<t, a> temp;
		temp.resize( i_lhs.size() + i_rhs.size() );// the largest it could be is the sum of both sizes
		auto newEnd = std::set_union( i_lhs.cbegin(), i_lhs.cend(), i_rhs.cbegin(), i_rhs.cend(), temp.begin(), i_lhs.get_compare() );
		temp.resize( std::distance( temp.begin(), newEnd ) );

		return temp; // RVO
	}

	//--------------------------------
	/* set_difference( i_lhs, i_rhs )
		O( 2(n+m)-1 )
			where:
				n = i_lhs.size()
				m = i_rhs.size()

		NOTE: used in - and -=
	*/
	template< typename t, typename c, typename e, typename a>
	inline
	std::vector<t, a>
	set_difference( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
	{
		assert( i_lhs.empty() == false && i_rhs.empty() == false );

		std::vector<t, a> temp;
		temp.resize( i_lhs.size() );// the largest it could be is size of lhs
		auto newEnd = std::set_difference( i_lhs.cbegin(), i_lhs.cend(), i_rhs.cbegin(), i_rhs.cend(), temp.begin(), i_lhs.get_compare() );
		temp.resize( std::distance( temp.begin(), newEnd ) );

		return temp; // RVO
	}

	//--------------------------------
	/* set_intersection( i_lhs, i_rhs )
		O( 2(n+m)-1 )
			where:
				n = i_lhs.size()
				m = i_rhs.size()

		NOTE: used in & and &=
	*/
	template< typename t, typename c, typename e, typename a>
	inline
	std::vector<t, a>
	set_insertection( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
	{
		assert( i_lhs.empty() == false && i_rhs.empty() == false );

		std::vector<t, a> temp;
		temp.resize( std::min( i_lhs.size(), i_lhs.size() ) ); // the max size it could be is the min of both sets
		auto newEnd = std::set_intersection( i_lhs.cbegin(), i_lhs.cend(), i_rhs.cbegin(), i_rhs.cend(), temp.begin(), i_lhs.get_compare() );
		temp.resize( std::distance( temp.begin(), newEnd ) );

		return temp; // RVO
	}

	//--------------------------------
	/* set_symmetric_difference( i_lhs, i_rhs )
		O( 2(n+m)-1 )
			where:
				n = i_lhs.size()
				m = i_rhs.size()

		NOTE: used in ^ and ^=
	*/
	template< typename t, typename c, typename e, typename a>
	inline
	vector<t, a>
	set_symmetric_difference( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
	{
		assert( i_lhs.empty() == false && i_rhs.empty() == false );

		std::vector<t, a> temp;
		temp.resize( i_lhs.size() + i_rhs.size() ); // the max size it could be is the sum of both sets
		auto newEnd = std::set_symmetric_difference( i_lhs.cbegin(), i_lhs.cend(), i_rhs.cbegin(), i_rhs.cend(), temp.begin(), i_lhs.get_compare() );
		temp.resize( std::distance( temp.begin(), newEnd ) );

		return temp; // RVO
	}

	//-------------------------------- functions for comparison operators --------------------------------

	//--------------------------------
	/* equal( i_lhs, i_rhs )
		O(n)
			where:
				n = i_lhs.size() = i_rhs.size()

		NOTE: used in == and !=
	*/
	template< typename t, typename c, typename e, typename a>
	inline
	bool
	equal( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
	{
		assert( i_lhs.empty() == false && i_rhs.empty() == false );
		assert( i_lhs.size() == i_rhs.size() );

		return std::equal( i_lhs.cbegin(), i_lhs.cend(), i_rhs.cbegin(), i_rhs.cend(), i_lhs.get_equal() );
	}

	//--------------------------------
	/* subset( i_lhs, i_rhs )
		i_lhs subset of i_rhs
		O( 2(n+m)-1 )
			where:
				n = i_lhs.size()
				m = i_rhs.size()

		NOTE: a (subset) a == true
		NOTE: not proper subset
		NOTE: used in <,>, <= and>=
	*/
	template< typename t, typename c, typename e, typename a>
	inline
	bool
	subset( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
	{
		assert( i_lhs.empty() == false && i_rhs.empty() == false );

		auto itrL = i_lhs.cbegin();
		auto itrR = i_rhs.cbegin();
		auto itrLEnd = i_lhs.cend();
		auto itrREnd = i_rhs.cend();
		auto const& cmp = i_lhs.get_compare();
		do
		{
			auto& lval = *itrL;
			auto& rval = *itrR;

			if( cmp( lval, rval ) )
			{
				return false;
			}
			else if( cmp( rval, lval ) )
			{
				++itrR;
			}
			else
			{
				++itrL;
				++itrR;
			}

		} while( itrL != itrLEnd && itrR != itrREnd );
		
		// if the super-set is consumed but the sub-set is not consumed, its not a sub-set
		return (itrR == itrREnd && itrL != itrLEnd) == false;
	}

} // end of namespace

//--------------------------------
/* operator|=
	union / merge
	add both sets together, while
	avoiding duplicates in the result

	i.e. a U b

	NOTE: a U a == a
	NOTE: {empty} U a == a
	NOTE: a U {empty} == a
*/
template< typename t, typename c, typename e, typename a>
vec_set<t, c, e, a>& operator|=( vec_set<t, c, e, a>& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	if( &i_lhs == &i_rhs || i_rhs.empty() )
	{
		return i_lhs;
	}
	else if( i_lhs.empty() )
	{
		i_lhs = i_rhs;
		return i_lhs;
	}

	auto temp = detail_vec_set_operators::set_union( i_lhs, i_rhs );
	i_lhs._move_external_cont_to_internal_cont( std::move( temp ) );

	return i_lhs;
}

//----------------
/* operator|
	same as above
*/
template< typename t, typename c, typename e, typename a>
vec_set<t, c, e, a> operator|( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	if( &i_lhs == &i_rhs || i_rhs.empty() )
	{
		return vec_set<t, c, e, a>( i_lhs );
	}
	else if( i_lhs.empty() )
	{
		return vec_set<t, c, e, a>( i_rhs );
	}

	auto temp = detail_vec_set_operators::set_union( i_lhs, i_rhs );

	vec_set<t, c, e, a> result;
	result._move_external_cont_to_internal_cont( std::move( temp ) );

	return result;
}

//--------------------------------
/* operator-=
	subtraction / compliment / difference
	i.e. a - (a U b)

	NOTE: a - a == {empty}
	NOTE: {empty} - a == {empty}
	NOTE: a - {empty} == a
*/
template< typename t, typename c, typename e, typename a>
vec_set<t, c, e, a>& operator-=( vec_set<t, c, e, a>& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	if( &i_lhs == &i_rhs )
	{
		i_lhs.clear();
		return i_lhs;
	}
	else if( i_lhs.empty() || i_rhs.empty() )
	{
		return i_lhs;
	}

	auto temp = detail_vec_set_operators::set_difference( i_lhs, i_rhs );
	i_lhs._move_external_cont_to_internal_cont( std::move( temp ) );

	return i_lhs;
}

//----------------
/* operator-
	same as above
*/
template< typename t, typename c, typename e, typename a>
vec_set<t, c, e, a> operator-( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	if( &i_lhs == &i_rhs )
	{
		return vec_set<t, c, e, a>();
	}
	else if( i_lhs.empty() || i_rhs.empty() )
	{
		return vec_set<t, c, e, a>( i_lhs );
	}

	auto temp = detail_vec_set_operators::set_difference( i_lhs, i_rhs );
	vec_set<t, c, e, a> result;
	result._move_external_cont_to_internal_cont( std::move( temp ) );
	
	return result;
}

//--------------------------------
/* operator&=
	intersection, the values that are in both sets i.e. a n b

	NOTE: a & a == a
	NOTE: {empty} & a == {empty}
	NOTE: a & {empty} == {empty}
*/
template< typename t, typename c, typename e, typename a>
vec_set<t, c, e, a>& operator&=( vec_set<t, c, e, a>& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	if( &i_lhs == &i_rhs )
	{
		return i_lhs;
	}
	else if( i_lhs.empty() || i_rhs.empty() )
	{
		i_lhs.clear();
		return i_lhs;
	}

	auto temp = detail_vec_set_operators::set_insertection( i_lhs, i_rhs );
	i_lhs._move_external_cont_to_internal_cont( std::move( temp ) );

	return i_lhs;
}

//----------------
/* operator&
	same as above
*/
template< typename t, typename c, typename e, typename a>
vec_set<t, c, e, a> operator&( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	if( &i_lhs == &i_rhs )
	{
		return vec_set<t, c, e, a>( i_lhs );
	}
	else if( i_lhs.empty() || i_rhs.empty() )
	{
		return vec_set<t, c, e, a>();
	}

	auto temp = detail_vec_set_operators::set_insertection( i_lhs, i_rhs );
	vec_set<t, c, e, a> result;
	result._move_external_cont_to_internal_cont( std::move( temp ) );

	return result;
}

//--------------------------------
/*  operator^=
	symmetric difference a bit like exclusive or, its in either set but NOT in both
	i.e. a ^ b or (a union b) subtract (a intersection b)

	NOTE: a ^ a == {empty}
	NOTE: {empty} ^ a == a
	NOTE: a ^ {empty} == a
*/
template< typename t, typename c, typename e, typename a>
vec_set<t, c, e, a>& operator^=( vec_set<t, c, e, a>& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	if( &i_lhs == &i_rhs )
	{
		i_lhs.clear();
		return i_lhs;
	}
	else if( i_lhs.empty() )
	{
		i_lhs = i_rhs;
		return i_lhs;
	}
	else if( i_rhs.empty() )
	{
		return i_lhs;
	}

	auto temp = detail_vec_set_operators::set_symmetric_difference( i_lhs, i_rhs );
	i_lhs._move_external_cont_to_internal_cont( std::move( temp ) );

	return i_lhs;
}

//----------------
/*  operator^
	same as above
*/
template< typename t, typename c, typename e, typename a>
vec_set<t, c, e, a> operator^( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	if( &i_lhs == &i_rhs )
	{
		return vec_set<t, c, e, a>();
	}
	else if( i_lhs.empty() )
	{
		return vec_set<t, c, e, a>( i_rhs );
	}
	else if( i_rhs.empty() )
	{
		return vec_set<t, c, e, a>( i_lhs );
	}

	auto temp = detail_vec_set_operators::set_symmetric_difference( i_lhs, i_rhs );
	vec_set<t, c, e, a> result;
	result._move_external_cont_to_internal_cont( move( temp ) );

	return result;
}

//-------------------------------- comparison operators --------------------------------
/*
	operator == : checks for equality (both are same size, and all elements match)
	operator != : check for inequality
	operator < : checks if left is a proper sub-set of right.
	operator > : checks if left is a proper super-set of right.
	operator <= : checks if left is a sub-set of right (every element of left is in right).
	operator >= : checks if left is a super-set of right (every element of right is in left).
*/

//--------------------------------
/* operator==
*/
template< typename t, typename c, typename e, typename a>
bool operator==( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	// check if they point to the same object
	if( &i_lhs == &i_rhs ) return true;

	// check sizes
	if( i_lhs.size() != i_rhs.size() ) return false;

	// we have determined they both have the same size, however, both are empty
	if( i_lhs.empty() ) return true;

	return detail_vec_set_operators::equal( i_lhs, i_rhs );
}

//--------------------------------
/* operator!=
*/
template< typename t, typename c, typename e, typename a>
bool operator!=( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	// check if they point to the same object
	if( &i_lhs == &i_rhs ) return false;

	// check sizes
	if( i_lhs.size() != i_rhs.size() ) return true;

	// we have determined they both have the same size, however, both are empty
	if( i_lhs.empty() ) return false;

	return detail_vec_set_operators::equal( i_lhs, i_rhs ) == false;
}

//--------------------------------
/* operator<
	is i_lhs a proper sub-set of i_rhs

	NOTE: a (sub-set) a == false
	NOTE: {} (sub-set) {} == false
	NOTE: {} (sub-set) a == true
	NOTE: sub-set, not proper subset i.e. equal is still a sub-set
*/
template< typename t, typename c, typename e, typename a>
bool operator<( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	// check sizes (will also discard checking against self)
	if( i_lhs.size() == i_rhs.size() ) return false;

	// empty is a sub-set of any set (we know that i_rhs is not the same size at this point)
	if( i_lhs.empty() ) return true;

	// check sizes (if left is bigger then right, it can't be a sub-set)
	if( i_lhs.size() > i_rhs.size() ) return false;

	return detail_vec_set_operators::subset( i_lhs, i_rhs );
}

//--------------------------------
/* operator>
	is i_lhs a proper super-set of i_rhs
*/
template< typename t, typename c, typename e, typename a>
bool operator>( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	// check sizes (will also discard checking against self)
	if( i_lhs.size() == i_rhs.size() ) return false;

	// empty is a sub-set of any set (we know that i_lhs is not the same size at this point)
	if( i_rhs.empty() ) return true;

	// check sizes (if left is bigger then right, it can't be a sub-set)
	if( i_rhs.size() > i_lhs.size() ) return false;

	return detail_vec_set_operators::subset( i_rhs, i_lhs );
}

//--------------------------------
/* operator<=
	returns true if i_lhs is a sub-set of i_rhs
	i.e. every value in i_lhs in included in i_rhs and
	they can be the same size (totally equal).

	NOTE: a (sub-set) a == true
	NOTE: {} (sub-set) {} == true
	NOTE: {} (sub-set) a == true
	NOTE: sub-set, not proper subset i.e. equal is still a sub-set
*/
template< typename t, typename c, typename e, typename a>
bool operator<=( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	// check if they point to the same object
	if( &i_lhs == &i_rhs ) return true;

	// both are empty
	if( i_lhs.empty() && i_rhs.empty() ) return true;

	// empty is a sub-set of any set
	if( i_lhs.empty() ) return true;

	// check sizes (if left is bigger then right, it can't be a sub-set)
	if( i_lhs.size() > i_rhs.size() ) return false;

	return detail_vec_set_operators::subset( i_lhs, i_rhs );
}

//--------------------------------
/* operator>=
	returns true if i_lhs is a super-set of i_rhs
	i.e. every value in i_rhs in included in i_lhs and
	they can be the same size (totally equal).

	NOTE: a (super-set) a == true
	NOTE: {} (super-set) {} == true
	NOTE: a (super-set) {} == false
	NOTE: super-set, not proper super-set i.e. equal is still a super-set
*/
template< typename t, typename c, typename e, typename a>
bool operator>=( vec_set<t, c, e, a> const& i_lhs, vec_set<t, c, e, a> const& i_rhs )
{
	// check if they point to the same object
	if( &i_lhs == &i_rhs ) return true;

	// both are empty
	if( i_lhs.empty() && i_rhs.empty() ) return true;

	// empty is not a super-set of any set
	if( i_lhs.empty() ) return false;

	// check sizes (if left is bigger then the right, it can't be a super-set)
	if( i_lhs.size() < i_rhs.size() ) return false;

	return detail_vec_set_operators::subset( i_rhs, i_lhs );
}

//-------------------------------- ostream operator --------------------------------

//--------------------------------
/* ostream operator <<
	Nicely formats the data to a ostream which is usually the cout ostream.

	For example the code:
		vec_set<size_t> vs{1,2,3,4}; cout << vs << endl;
	prints:
		[1,2,3,4]

	NOTE: remember your data type needs an implementation of ostream operator too
*/
template< typename t, typename c, typename e, typename a>
std::ostream& operator<< ( std::ostream& o_stream, vec_set<t, c, e, a> const& i_vec_set )
{
	o_stream << '[';
	auto count = i_vec_set.size();
	if( count > 0 )
	{
		std::cout << i_vec_set.front();

		if( count > 1 )
		{
			auto cur = i_vec_set.begin();
			auto end = i_vec_set.end();
			++cur; // we have already done the first element (above)
			do
			{
				o_stream << ',' << *cur;
			} while( ++cur != end );
		}
	}
	o_stream << ']' << std::endl;
	return o_stream;
}

//--------------------------------
/* operator>>
	Given a correctly formatted string eg "[1,2,4]", this
	function will create a set from that string.

	NOTE: can throw exceptions.
	NOTE: doesn't vigorously check syntax of the string.
	NOTE: depends on the functions: "Tonkenize" from sys_stringUtils and "type_to_type" from sys_global
*/
#if 1
template< typename t, typename c, typename e, typename a>
std::istream& operator>>( std::istream& i_stream, vec_set<t, c, e, a>& o_set )
{
	//-------- setup source --------
	std::string source;
	i_stream >> source;

	//-------- split the string --------
	std::vector<string> split;
	Tokenize( source, ",", split );

	//-------- try extract each value --------
	std::stringstream ss;
	for( auto& element : split )
	{
		t const val = type_to_type<std::string, t>( element, ss );
		o_set.insert( val );
	}
	return i_stream;
}
#endif

#endif
