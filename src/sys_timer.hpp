/*	This file is part of sorted_vector_as_set.
	Copyright(C) 2016 by Karl Wesley Hutchinson

	sorted_vector_as_set is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	sorted_vector_as_set is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with sorted_vector_as_set. If not, see <http://www.gnu.org/licenses/>.
*/

#if !defined (D_SYS_TIMER_HPP)
#define D_SYS_TIMER_HPP

#include <chrono>
#include <assert.h>

//----------------------------------------------------------------
/* Timer
Simple timer class using C++11 chrono.
example use:
{
	Timer_t t; // use global typedef for standard timer..
	t.Start(); // start the timer.
	... do something you want to time ...
	t.End();   // end the timer
	t.CalcDuration; // Calculate and store (inside the Timer class) the interval between start and end.
	cout << "Time in milliseconds: " << t.GetDuration().count() << end; // cout the interval
}
NOTE: End is in a separate function to CalcDuration to attempt to improve accuracy.
*/
template< typename precision = double, typename ratio = std::milli >
class Timer
{
public:
	typedef std::chrono::time_point<std::chrono::system_clock>	timePoint_t;
	typedef std::chrono::duration<precision, ratio>				timeDuration_t;

	Timer() :m_start(), m_end() {}

	void Start()
	{
		m_start = std::chrono::system_clock::now();
	}

	void End()
	{
		m_end = std::chrono::system_clock::now();
	}

	void CalcDuration()
	{
		assert(m_start<=m_end);
		m_duration = (m_end - m_start);
	}

	timeDuration_t const& GetDuration() const
	{
		return m_duration;
	}

	static timeDuration_t TestLatency( size_t const i_count = 128)
	{
		Timer<precision, ratio> t;
		timeDuration_t tSum;
		for (size_t i = 0; i < i_count; ++i)
		{
			t.Start();
			t.End();
			t.CalcDuration();
			tSum += t.GetDuration();
		}
		return tSum / i_count;
	}

private:
	timePoint_t		m_start;
	timePoint_t		m_end;
	timeDuration_t	m_duration;
};

typedef Timer<> Timer_t;

#endif
